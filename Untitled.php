<?php

?>

<header class="main-header mega-header">
		<div class="container clearfix">
			<div class="menu-overlay-for-body"></div>
			<div class="logo-area">
				<a class="phone-menu-trigger" href="#" aria-haspopup="true">
					<i class="fas fa-bars"></i>
				</a>
				<a href="index.php">
					<img src="assets/img/header/logo.png" alt="Multimo" width="169" height="22">
				</a>
			</div>
			<!--
			<div class="additional-buttons">
				<form class="main-menu-search-form clearfix" method="get" action="search-result">
					<div class="stylish-big-search-form">
								<input type="search" id="search" class="main-search-input" placeholder="Search for" name="search" autofocus  onKeyPress="return submitenter(this,event)"/>

					</div>
											<button type="submit" class="search-trigger">
													<i class="fa fa-search"></i>
											</button>
				</form>

			</div>
			-->

			<div class="additional-buttons">
				<form class="main-menu-search-form clearfix" method="post">
					<!-- <div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div> -->
					<input type="hidden" name="category" id="filter-category">
					<input type="hidden" name="type" id="filter-type">
					<input type="hidden" name="color" id="filter-color">
					<input type="hidden" name="price-lower" id="filter-price-lower">
					<input type="hidden" name="price-higher" id="filter-price-higher">
					<button type="submit" class="search-trigger">
						<i class="fa fa-search"></i>
					</button>
					<div class="stylish-big-search-form">
						<input type="search" class="main-search-input" placeholder="Search for">
						<span class="search-category-type">
							- "<span class="the-type"></span><span class="the-category"></span>"
						</span>
						<span class="search-hidden-width"></span>
						<div class="filter-trigger">
							<span class="change-filter-trigger-button">
								filters
							</span>
							<i class="fa fa-chevron-down"></i>
						</div>
						<div class="search-filter-changes">
							<span class="filter-color">
								<span class="the-color"></span>
							</span>
							<span class="search-filter-prices clearfix">
								<span class="filter-lower-price"></span> <span>-</span> <span class="filter-higher-price"></span>
							</span>
						</div>
						<div class="filter-box-container">
							<div class="filter-box">
								<ul class="nav nav-pills">
									<li role="presentation" class="active" >
										<a href="contact.html#category" aria-controls="category" role="tab" data-toggle="tab">category</a>
									</li>
									<li role="presentation" class="disabled">
										<a href="contact.html#type" aria-controls="type" role="tab" data-toggle="tab">type</a>
									</li>
									<li role="presentation">
										<a href="contact.html#color" aria-controls="color" data-toggle="tab">color</a>
									</li>
									<li role="presentation">
										<a href="contact.html#price" aria-controls="price" role="tab" data-toggle="tab">price</a>
									</li>
								</ul>
								<div class="tab-content">
									<h5>
										<span class="filter-title-placeholder">
											select category
										</span>
										<span class="filter-category">

										</span>
										<span class="filter-type">

										</span>
										<span class="filter-color">
											<span class="the-color"></span>
										</span>
										<span class="filter-price">
											- <span class="filter-lower-price"></span> - <span class="filter-higher-price"></span>
										</span>
									</h5>
									<div role="tabpanel" class="tab-pane fade in active" id="category">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default">
												<div class="panel-heading" role="tab">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="contact.html#woman" aria-expanded="true" class="collapsed" aria-controls="woman">woman</a>
													</h4>
												</div>
												<div id="woman" class="panel-collapse collapse" role="tabpanel">
													<div class="panel-body">
														<ul class="search-filter-list filter-category-list">
															<li>
																<a href="contact.html#">Jackets</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
															<li>
																<a href="contact.html#">Blazers</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
															<li>
																<a href="contact.html#">Suits</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
															<li>
																<a href="contact.html#">Trousers</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
															<li>
																<a href="contact.html#">Jeans</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
															<li>
																<a href="contact.html#">Shirts</a>
																<ul>
																	<li>
																		<a href="contact.html#">Pliester</a>
																	</li>
																	<li>
																		<a href="contact.html#">Carton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Cotton</a>
																	</li>
																	<li>
																		<a href="contact.html#">Aliminium</a>
																	</li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="contact.html#man" aria-expanded="true" class="collapsed" aria-controls="man">
															man
														</a>
													</h4>
												</div>
												<div id="man" class="panel-collapse collapse" role="tabpanel">
													<div class="panel-body">
														<ul class="search-filter-list filter-category-list">
															<li>
																<a href="contact.html#">
																	Jackets
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Blazers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Suits
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Trousers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Jeans
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Shirts
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="contact.html#kids" aria-expanded="true" class="collapsed" aria-controls="kids">
															kids
														</a>
													</h4>
												</div>
												<div id="kids" class="panel-collapse collapse" role="tabpanel">
													<div class="panel-body">
														<ul class="search-filter-list filter-category-list">
															<li>
																<a href="contact.html#">
																	Jackets
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Blazers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Suits
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Trousers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Jeans
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Shirts
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="contact.html#shoes-bags" aria-expanded="true" class="collapsed" aria-controls="shoes-bags">
															shoes & bags
														</a>
													</h4>
												</div>
												<div id="shoes-bags" class="panel-collapse collapse" role="tabpanel">
													<div class="panel-body">
														<ul class="search-filter-list filter-category-list">
															<li>
																<a href="contact.html#">
																	Jackets
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Blazers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Suits
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Trousers
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Jeans
																</a>
															</li>
															<li>
																<a href="contact.html#">
																	Shirts
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div role="tabpanel" class="tab-pane" id="type">
										<ul class="search-filter-list filter-type-list">
											<li>
												<a href="contact.html#">
													Jackets
												</a>
											</li>
											<li>
												<a href="contact.html#">
													Blazers
												</a>
											</li>
											<li>
												<a href="contact.html#">
													Suits
												</a>
											</li>
											<li>
												<a href="contact.html#">
													Trousers
												</a>
											</li>
											<li>
												<a href="contact.html#">
													Jeans
												</a>
											</li>
											<li>
												<a href="contact.html#">
													Shirts
												</a>
											</li>
										</ul>
									</div>
									<div role="tabpanel" class="tab-pane" id="color">
										<ul class="color-list">
											<li class="active">
												<a href="contact.html#" data-color="all" class="multicolor" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #1476f1;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #82d889;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #3c3d41;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #fb5656;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #556791;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #82d889;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #ce954a;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #777777;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #ff95ce;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #f1c014;" aria-controls="color" data-toggle="tab"></a>
											</li>
											<li>
												<a href="contact.html#" data-color="blue" style="background-color: #ffecb2;" aria-controls="color" data-toggle="tab"></a>
											</li>
										</ul>
									</div>
									<div role="tabpanel" class="tab-pane" id="price">
										<div class="price-slider-filter-container clearfix">
											<div class="price-slider-filter" data-min-price="15" data-max-price="650" data-curent-min="150" data-curent-max="500"></div>
											<input type="text" class="lower-price" readonly>
											<input type="text" class="higher-price" readonly>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="my-cart-link" aria-haspopup="true">
					<a>
						<span>3</span>
						<i class="fa fa-shopping-cart"></i>
					</a>
					<div class="header-cart-container">
						<div class="header-cart">
							<div class="cart-head clearfix">
								<p>
									You’r Shopping Cart
								</p>
								<p class="prod-num">
									<span>
										2
									</span>
									Product/s
								</p>
							</div>
							<div class="cart-entries">
								<figure>
									<a href="contact.html#" class="product-thumbnail">
										<img src="assets/img/shop/header-product-01.jpg" alt="">
									</a>
									<figcaption>
										<span>
											jackets
										</span>
										<a href="contact.html#">
											sport jacket brown / blue
										</a>
										<p class="cart-prod-details">
											48
											<span class="color-cart" style="background-color: #002aff;"></span>
											1 pcs
										</p>
										<p class="price">
											$55,<sup>99</sup>
										</p>
										<div class="right-buttons">
											<a href="contact.html#">
												<i class="fa fa-times"></i>
											</a>
											<a href="contact.html#">
												<i class="fa fa-heart"></i>
											</a>
										</div>
									</figcaption>
								</figure>
								<figure>
									<a href="contact.html#" class="product-thumbnail">
										<img src="assets/img/shop/header-product-02.jpg" alt="">
									</a>
									<figcaption>
										<span>
											jackets
										</span>
										<a href="contact.html#">
											sport jacket brown / blue
										</a>
										<p class="cart-prod-details">
											48
											<span class="color-cart" style="background-color: #002aff;"></span>
											1 pcs
										</p>
										<p class="price">
											$55,<sup>99</sup>
										</p>
										<div class="right-buttons">
											<a href="contact.html#">
												<i class="fa fa-times"></i>
											</a>
											<a href="contact.html#">
												<i class="fa fa-heart"></i>
											</a>
										</div>
									</figcaption>
								</figure>
								<figure>
									<a href="contact.html#" class="product-thumbnail">
										<img src="assets/img/shop/header-product-03.jpg" alt="">
									</a>
									<figcaption>
										<span>
											jackets
										</span>
										<a href="contact.html#">
											sport jacket brown / blue
										</a>
										<p class="cart-prod-details">
											48
											<span class="color-cart" style="background-color: #002aff;"></span>
											1 pcs
										</p>
										<p class="price">
											$55,<sup>99</sup>
										</p>
										<div class="right-buttons">
											<a href="contact.html#">
												<i class="fa fa-times"></i>
											</a>
											<a href="contact.html#">
												<i class="fa fa-heart"></i>
											</a>
										</div>
									</figcaption>
								</figure>
							</div>
							<div class="cart-footer clearfix">
								<p class="total">
									<span>
										subtotal
									</span>
									$153.<sup>56</sup>
								</p>
								<div class="cart-buttons">
									<a href="contact.html#" class="btn">
										view cart
									</a>
									<a href="contact.html#" class="checkout btn btn-primary">
										checkout
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--
			<div class="header-user-options">
				<div class="user-sign-in">
					<i class="fa fa-user"></i>
					<a href="faq.php"><span class="text-header-span">help desk</span></a>
				</div>
			</div>
			<div class="header-user-info">
				<div class="info">
					<i class="fa fa-phone"></i>
					<a href="tel:+62318544449">+62 31 85 4444 9</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="menu-container clearfix">
			-->

				<nav class="menu-area">
					<div class="phone-logo">
						<a href="index.php">
							<img src="assets/img/header/logo.png" alt="" width="169" height="22">
						</a>
					</div>
					<ul class="clearfix">
						<li>
							<a href="index.php">
								home
							</a>
						</li>
						<li>
							<a href="about.php">
								About
							</a>

						</li>
						<li>
							<a href="#">
								Product
							</a>

							<div class="main-menu-dropdown big-mega-menu light">
								<div class="mega-menu-container clearfix">
									<div class="col-sm-7 header-col">
										<div class="row">
											<div class="col-sm-4 header-col">
												<ul>
													<li>
														<a href="#">
															By Category
														</a>
														<ul>
															<li>
																	<a href="by-category.php">
																			Office
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			Home
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			School
																	</a>
															</li>
															<li>
																	<a href="chair.php">
																			Chair
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			Table
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			Restaurant
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			Custom Project
																	</a>
															</li>
															<li>
																	<a href="by-category.php">
																			Wall Decoration
																	</a>
															</li>
														</ul>
													</li>
												</ul>
											</div>
											<div class="col-sm-4 header-col">
												<ul>
													<li>
														<a href="#">
															By Brand
														</a>
														<ul>
															<li>
																	<a href="seahorse.php">
																			<img src="assets/img/brand/seahorse.png" height="60">
																	</a>
															</li>
															<li>
																	<a href="by-brand.php">
																			<img src="assets/img/brand/classmate.png" height="60">
																	</a>
															</li>
															<li>
																	<a href="by-brand.php">
																			<img src="assets/img/brand/goodliving.png" height="60">
																	</a>
															</li>
															<li>
																	<a href="by-brand.php">
																			<img src="assets/img/brand/modenta.png" height="60">
																	</a>
															</li>
														</ul>
													</li>
													<li style="visibility: hidden">
														<a href="#">
															Furniture
														</a>
														<ul>
															<li>
																<a href="#">
																	Furniture Item
																</a>
															</li>
														</ul>
													</li>
												</ul>
											</div>
											<div class="col-sm-4 header-col">
												<ul>
													<li>
														<a href="#">
															Media
														</a>

														<ul>
															<li>
																<a href="assets/media/Promo-Cash-Back.jpg" >
																		Modenta UV Print 2016
																</a>
																<a href="assets/media/Promo-Cash-Back.jpg" >
																		Multimo Funtastic Promo
																</a>
																<br>
																<a href="assets/media/Promo-Cash-Back.jpg">
																		Promo Cash Back
																</a>
																<br>
															</li>
														</ul>
													</li>
												</ul>
											</div>

										</div>
									</div>
									<div class="col-sm-5 header-col">
										<div class="owl-collesction-header right">
											<div class="menu-collection-box style-03" style="background-image: url('assets/img/product/Blueberry 1.jpg');"></div>
										</div>
									</div>
								</div>
							</div>

						</li>
						<li>
							<a href="project.php">
								Project Gallery
							</a>
						</li>
						<li>
							<a href="news-list.php">
								News and Tips
							</a>
						</li>

						<li>
							<a href="#">
								Contact
							</a>
							<ul>
							  <li>
								<a href="contact.php">Contact</a>
								</li>
								<li>
								<a href="request-sample.php">Request Sample</a>
								</li>
								<li>
								<a href="warranty-check.php">Warranty Check</a>
								</li>
								<li>
								<a href="opportunity.php">Business Opportunity</a>
								</li>
								<li>
								<a href="job-vacancy.php">Job Vacancy</a>
								</li>
							</ul>
						</li>
					</ul>
					<ul class="social-icons-phone">
						<li>
							<a href="https://www.instagram.com/multimo_furniture/">
								<i class="fa fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/MultimoFurniture/">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/MultimoRd2">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>

	<SCRIPT TYPE="text/javascript">
		 /*function submitenter(myfield,e)
		 {
		 	var keycode;
			if (window.event)
				keycode = window.event.keyCode;
			else if (e)
				keycode = e.which;
			else
				return true;
			if (keycode == 13)
			{
				myfield.form.submit();
				return false;
			}
			else
				return true;
		} //--> */
		 function submitenter(myfield,e)
		 {
			 var keycode;
			 if (window.event)
				 keycode = window.event.keyCode;
			 else if (e)
				 keycode = e.which;
			 else
				 return true;

			 if (keycode == 13) {
				 $search = $('#search').val()
				window.location.replace(" search-result/" + $search);

			 }
		 } //-->

		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete?");
		  if (x)
			  return true;
		  else
			return false;
		}
        </SCRIPT>
