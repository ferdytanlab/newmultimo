<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Frequently Ask Questions in Multimo.">
	<meta name="keywords" content="PT. Multi Modern Nusantara, Furniture Industrial, Experience, History, Corporate, Vision, Mission, Commitment, Value, About Us, Strong, Durable, Quality, warranty">
	<title>F.A.Q - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>
	<br><br>
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4>
						F.A.Q
					</h4>
				</div>

                <div class="col-sm-12">
					<h3>
						What is Stainless Steel?
					</h3>
                    <p>
                    	Stainless steel is an alloy of Iron with a minimum of 10.5% Chromium. Produces a thin layer of oxide on the surface of the steel known as the 'passive layer'. This prevents any further corrosion of the surface. Increasing the amount of Chromium gives an increased resistance to corrosion. Therefore, Stainless Steel is so much more Strong and Durable Rather than Steel with the chrome process.
                    </p>
				</div>

                <div class="col-sm-12">
					<h3>
						Where can I find the dimensions of a piece of furniture?
					</h3>
                    <p>
                    	Dimensions can be seen on the Product detail page in the right section under the Product Name and Product Description. Please <a href="http://multimo.co.id/product-detail.php?id=34">click here</a> for an example.
                    </p>
				</div>

                <div class="col-sm-12">
					<h3>
						Why are there no prices shown on your web site?
					</h3>
                    <p>
                    	The multimo.co.id web site is a manufacturer's web site designed to showcase our furniture products. Prices are not shown on this site because Multimo Furniture Home Stores and dealers are independently owned and operated. To obtain pricing please visit your nearest Multimo Furniture HomeStore or dealer.</p>
                        <p>
The Multimo Furniture <a href="contact.php#stockist">Stockist</a> tool will help you find the nearest Multimo Furniture HomeStore or dealer.</p>
                    </p>
				</div>

                <div class="col-sm-12">
					<h3>
						Can I get an Multimo Furniture catalog?
					</h3>
                    <p>
                    	we have catalogs or brochures that can be sent directly to consumers. Our catalogs are available for viewing on this site.
 The Multimo Furniture <a href="http://multimo.co.id/assets/media/Catalogue%20Multimo.pdf">Catalog</a> tool will help you find the catalog and automatic downloaded.
</p>

				</div>

                <div class="col-sm-12">
					<h3>
						How can I apply for a job at Multimo Furniture?
					</h3>
                    <p>
                    	<a href="job-vacancy.php">Click here</a> for information regarding current job openings at Multimo Furniture Industries. You may also submit an online application with email. If you are interested in employment at an Multimo Furniture HomeStore, please contact the individual store.

</p>

				</div>
                <div class="col-sm-12">
					<h3>
						Who can answer my warranty question?
					</h3>
                    <p>
                    	All claims must include the original bill of sale, the product serial number, and be filed within the applicable warranty period. If you need service, contact Multimo in writing or call our Customer Care <a href="contact.php">Click Here</a>.

</p>

				</div>

			</div>
		</div>
	</div>






<br><br>

	<?php include ('footer.php') ?>
