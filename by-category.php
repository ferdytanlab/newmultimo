<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Product lists from categoryName category.">
	<meta name="keywords" content="categoryName, category, product, multimo">
	<title>categoryName - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>

	<div class="section-padding" style="background-image: url('assets/img/category/CoverCategoryChair.jpg');">
		<div class="container">
			<div class="banner">
				<h3>
					Category
				</h3>

			</div>
		</div>
		<div class="section-overlay" ></div>
	</div>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mini-breadcrumb">

					</div>
				</div>

				<div class="col-sm-12 multiproduct">



         </div>
				</div>
			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
