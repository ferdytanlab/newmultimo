<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Modenta adalah brand Decorative UV Printing dari perusahaan MULTIMO yang  siap untuk mendukung dan menjadikan usaha Anda lebih unik dan kreatif serta membuat dekorasi ruangan rumah Anda lebih menarik. Wujudkan imajinasi Anda menjadi nyata dengan bantuan kami.">
	<meta name="keywords" content="UV Printing, UV print Surabaya, UV Print Sidoarjo, Decorative Printing, Print Akrilik, Print Kayu, Print Kain, Print Keramik, Print Kaca, Print Casing HP, Print Besi, Print Batu, Print Meja, Print Jendela, Print Apa Saja, Print, Printing, Print Lengkap, Print Kulit, Print Mika, Print Plat, Custom Printing, Modenta, Modenta Decorative UV Printing, Ultraviolet, Print Canggih.">

	<title>MODENTA - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->

	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>

	<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>



	 <img src="assets/img/modenta/modenta.jpg">

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<br><br>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-1.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-2.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-3.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-4.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-5.jpg">
				</div>

				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-6.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-7.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-8.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-9.jpg">
				</div>
				<div class="col-md-15 col-sm-3">
					<img src="assets/img/modenta/m-10.jpg">
				</div>

			</div>
		</div>
	</div>


	<div class="single-page-base-content" style="background-image: url(assets/img/modenta/bg-modenta.jpg); min-height: 1000px; background-repeat: no-repeat;" >
		<div class="container">
			<div class="row">
				<br><br><br><br>

				<div class="col-sm-1">
				</div>
				<div class="col-sm-4">
					<h3 style="color: white"> Decorative UV Printing</h3>
				<p style="color: white;">Lorem Ipsum</p>

				</div>
				<div class="col-sm-1">
				</div>
				<div class="col-sm-6">
					<img src="assets/img/modenta/modenta-desk.png">

				</div>
			</div>

			<div class="row">
				<br><br><br><br>

				<div class="col-sm-6">
					<img src="assets/img/modenta/modenta-hp.png">

				</div>
				<div class="col-sm-6">
					<h3> Printing untuk segala media</h3>
				<p style="color: black;">Lorem Ipsum</p>

				</div>
			</div>

			<div class="row">
				<br><br><br><br>

				<div class="col-sm-12">
					<div class="pull-right">
						<span>
							<a href="https://www.instagram.com/modentauvprint/">
								<i class="fab fa-instagram fa-3x" style="color: #9c2c20;"></i>
							</a>
						</span>

						<span style="margin-left: 15px;">
							<a href="https://www.facebook.com/Modenta-UV-Print-777474622375487/?fref=ts">
								<i class="fab fa-facebook fa-3x" style="color: #9c2c20;"></i>
							</a>
						</span>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="single-page-base-content" style="background-color: #9c2c20; height: 80px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
				<h3 style="color: white;"><i class="fab fa-whatsapp fa-lg"></i>  &nbsp;0838 4920 8080</h3>
					</div>
				</div>

			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
