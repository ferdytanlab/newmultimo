<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="We want you to succeed and we will help you make it happen. We offer you comprehensive support in order for you to provide your customers a truly unique and high-quality solution to their need.">
	<meta name="keywords" content="Lowongan Terbaru, Surabaya, Sidoarjo, Opportunity, Job Vacancy, Furniture, Distributor,  Kerja sama, Join, Multimo, facilities">
	<title>BUSINESS OPPORTUNITY - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>

	<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>
	 <img src="assets/img/content/opportunity.jpg">

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">

					<br><br><br><br>
					<img src="assets/img/content/partner.png">
					<br>

					<h3>
						Benefits of being a Multimo Partner
						<img src="assets/img/content/under.jpg">
					</h3>


				</div>
				<div class="col-sm-6">
					<p>We want you to succeed and we will help you make it happen. We offer you comprehensive support in order for you to provide your customers a truly unique and high-quality solution to their need.</p>

<p>We pay full respect to a mutual-benefit relationship based on loyalty, respect and care. Our focus is to give good-quality and innovative products with a reliable delivery, friendly customer service, non-stop education, responsive sales support and attractive marketing programs.</p>

				</div>
				<div class="col-sm-6">
					<ul style="list-style-type: none;">
						<li>
							<p style="font-size: 20px;">Highest Performing & Innovative Products</p>
							<p style="padding-top: 5px; padding-bottom: 15px;">
							We have a reputation as the industry pioneer to introduce leading edge products to market. The combination of innovation, quality and performance is unmatched in our industry. You will not be complained by introducing our products to your customers. Customers often do repeat order after enjoying the benefit of our products.
							</p>
						</li>

						<li>
							<p style="font-size: 20px;">Friendly relationship</p>
							<p style="padding-top: 5px; padding-bottom: 15px;">
							Multimo is a partner that brings real value. One of our focuses is to give our partner the best that we can. We are very happy to see our partner successful.
							</p>
						</li>

						<li>
							<p style="font-size: 20px;">Qualified Sales Leads</p>
							<p style="padding-top: 5px; padding-bottom: 15px;">
							Multimo generates qualified sales leads and passes them along directly to you.  Along with this, our partners can actively pursue opportunities through relationships and highly effective marketing efforts.
							</p>
						</li>

						<li>
							<p style="font-size: 20px;">Marketing Materials</p>
							<p style="padding-top: 5px; padding-bottom: 15px;">
							Multimo provides highly effective marketing tools along with aggressive marketing campaigns to create end-user awareness.  We develop excellent marketing materials for you.  Our marketing department will provide you with any assistance and materials required to create your own marketing material or your own website or your own social media platforms.
							</p>
						</li>

						<li>
							<p style="font-size: 20px;">Website</p>
							<p style="padding-top: 5px; padding-bottom: 15px;">
							Our website contains the latest industry information along with detailed product information and effective sales materials for end users. Along with this, we offer a link in our website to guide the customers to your location.
							</p>
						</li>
					</ul>
				</div>

				<div class="col-sm-12">
					<h3>
						Who wants to be our Partner?
						<img src="assets/img/content/under.jpg">
					</h3>
				</div>

				<div class="col-sm-6">
					<p>To become a Multimo partner, you must have been in furniture business for several years. Contact us today at <a href="tel:+62318544449">(031) 8544449</a> / <a href="tel:+62318544572">8544572</a> or complete the form below to explore an opportunity to a successful business-partnership.</p>

				</div>

				<div class="col-sm-6">

				<h3 style="margin-top: -25px;">Distribution Application</h3>

				        <form class="form" method="post">
	<table>

    <tr>
    <td class="name">
    	<label for="name">Business Name</label><br>
		<input type="text" name="name" id="name" />
    </td>
    </tr>

    <tr>
    <td class="address">
    	<label for="address">Address</label><br>
		<input type="text" name="address" id="address" />
    </td>
    </tr>

    <tr>
    <td class="email">
    	<label for="email">Email</label><br>
		<input type="text" name="email" id="email" />
    </td>
    </tr>

    <tr>
    <td class="phone">
    	<label for="phone">Phone</label><br>
		<input type="text" name="phone" id="phone" />
    </td>
    </tr>

    <tr>
    <td class="submit">
    <br>
    	<input id="submit" name="submit" type="submit" value="Submit Application"/>
    </td>
    </tr>
	</table>
				        </form>
        <?php
    $name = $_POST['name'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $from = "From: Multimo Application System" . "\r\n";
    $to = 'ferdy.think@yahoo.co.id';
    $subject = 'Multimo Distribution Application';

    $body = "From: $name\n \nAddress: $address\n  \nE-Mail: $email\n \nPhone: $phone";

    if ($_POST['submit']) {
        if (mail ($to, $subject, $body, $from)) {
	    echo 'Your message has been sent!';
	} else {
	    echo 'Something went wrong, go back and try again!';
	}

	}
?>

				</div>


			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
