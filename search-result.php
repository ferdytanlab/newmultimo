<?php
require_once 'admin/connect.php';
$search=mysqli_real_escape_string($conn,$_GET['search']);


//product terbaru
$sql="select p.id,p.productName,p.productDesc,p.productImage1,b.brandLogo from masterproduct p inner join masterbrand b on
      (p.productName like '%".$search."%' or p.productDesc like '%".$search."%') and b.id=p.brandid";
$queryProduct=$conn->query($sql) ;

//news
$sql="select id,newsTitle,newsContent,newsImage,newsDate from masternews where
newsTitle like '%".$search."%' or newsContent like '%".$search."%' ";
$queryNews=$conn->query($sql) ;
//----------------------------------

//project
$sql="select id,projectName,projectYear,projectImage1 from masterproject where
projectName like '%".$search."%' or projectName like '%".$search."%'";
$queryProject=$conn->query($sql) ;
//----------------------------------
//--------------------------------
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Don't hestitate to contact us, get in touch, and get direction.">
	<meta name="keywords" content="Location, Surabaya, Gedangan, Sidoarjo, East Java, Jawa Timur, Get in touch, Direction, Contact">
	<title>MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/master.css">
	<link rel="stylesheet" href="<?php echo BASE_URL;?>/assets/css/product.css">
	<link rel="shortcut icon" type="image/png" href="<?php echo BASE_URL;?>/assets/img/logoico.png"/>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77097361-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="4a77e4c6-1d52-4515-b15b-379d1374d6b8";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?> 
<!--
	<div class="section-padding dzsparallaxer auto-init" data-options='{mode_scroll:"normal",direction: "reverse",animation_duration: "1"}'>
		<div class="divimage dzsparallaxer--target" style="background-image: url('assets/img/slide-ecommerce-2.jpg');height: 150%;"></div>
		<div class="container">
			<div class="banner">
				<h3>
					Contact
				</h3>
			</div>
		</div>
		<div class="section-overlay" ></div>
	</div>
-->
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mini-breadcrumb">

					</div>
				</div>
				
				<div class="col-sm-12 multiproduct">
	
							<?php
                            while($row=$queryProduct->fetch_array()) {

                            ?>
					<div class="col-sm-4 multimo-product">
						<a href="<?php echo BASE_URL;?>/product-detail/<?php echo $row['id'];?>">
                            <img src="<?php echo BASE_URL;?>/assets/img/product/<?php echo $row['productImage1'];?>" width="215" height="215" class="attachment-shop_catalog wp-post-image" alt="<?php echo $row['productName'];?>">
                        </a>
                        
                        <div class="multimo-info">
							<span class="brand-product">
								<span class="the-product-status" onclick="window.location.href='<?php echo BASE_URL;?>/product-detail/<?php echo $row['id'];?>'" style="cursor:pointer;">
									<img align="middle" src="<?php echo BASE_URL;?>/assets/img/brand/<?php echo $row['brandLogo'];?>" width="75%">
								</span>
							</span>
                            <a href="<?php echo BASE_URL;?>/product-detail/<?php echo $row['id'];?>">
	                            <h3><?php echo $row['productName'];?></h3>
	                        </a>
                        </div>
                        
					</div>   
					<?php
                            }
                    ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="single-page-base-content">
		<div class="container">	
			<div class="row">
                <?php
                                while($row=$queryNews->fetch_array())
                                {
                                    $news=$row['newsContent'];
                                    $temp=substr($news, 0, 300);
                                    $temp.='.....';

                                    $tgl=$row['newsDate'];
                                    $tTgl = explode("-", $tgl);
                                    $tgl2=$tTgl[2].'/'.$tTgl[1].'/'.$tTgl[0];
                                    ?>
                                    <article>
                                        <header>
                                            <h2 style="font-size: 3.2rem">
                                                <a href="<?php echo BASE_URL;?>/news/<?php echo $row['id'];?>">
                                                    <?php echo $row['newsTitle'];?>
                                                </a>
                                            </h2>
                                            <div class="post-details">
                                                Posted by
                                                <a href="#">
                                                    Multimo
                                                </a>
                                                on <?php echo $tgl2;?>
                                            </div>
                                        </header>
                                        <figure>
                                            <a href="<?php echo BASE_URL;?>/news/<?php echo $row['id'];?>">
                                                <img src="<?php echo BASE_URL;?>/assets/img/news/<?php echo $row['newsImage'];?>" alt="">
                                            </a>
                                            <figcaption>
                                                <p>
                                                    <?php echo $temp;?>
                                                </p>
                                            </figcaption>
                                        </figure>
                                        <footer>
                                            <a href="<?php echo BASE_URL;?>/news/<?php echo $row['id'];?>" class="btn btn-primary">Read post</a>
                                        </footer>
                                    </article>
                                <?php
                                }
                                ?>

			</div>
		</div>
	</div>
	
	<div class="single-page-base-content">
		<div class="container">	
			<div class="row">
                <?php
                while($row=$queryProject->fetch_array())
                {
                    ?>
                    <div class="col-sm-6">
                        <figure class="collection-box align-center light" style="height: 163px; background-image: url('assets/img/project/<?php echo $row['projectImage1'];?>');">						<figcaption>
                                <p>
                                    <?php echo $row['projectYear'];?>
                                </p>
                                <h3>
                                    <?php echo $row['projectName'];?>
                                </h3>
                                <a href="<?php echo BASE_URL;?>/project-gallery/<?php echo $row['id'];?>" class="light">
                                    view projects
                                </a>
                            </figcaption>

                        </figure>
                    </div>
                <?php
                }
                ?>



			</div>
		</div>
	</div>

	<?php include ('footer.php') ?> 
	<script src="<?php echo BASE_URL;?>/assets/js/map.js"></script>


	<!--[if lte IE 9 ]>
		<script src="<?php echo BASE_URL;?>/assets/js/placeholder.js"></script>
		<script>
			jQuery(function() {
				jQuery('input, textarea').placeholder();
			});
		</script>
	<![endif]-->	
</body>
</html>