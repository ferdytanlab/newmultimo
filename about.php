<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="PT. Multi Modern Nusantara launched an umbrella brand “Multimo” to cover all other brands. Multimo are committed to make more innovation, to improve service and to provide guarantee for all its product so that not only it can serve its customer better but it can also contribute more to a better society.">
	<meta name="keywords" content="PT. Multi Modern Nusantara, Furniture Industrial, Experience, History, Corporate, Vision, Mission, Commitment, Value, About Us, Strong, Durable, Quality, warranty">
	<title>ABOUT MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<style>
	html body {
		background-color: white;
	}
	</style>
	<?php include ('css.php') ?>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>

	<br><br>
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<h4>
						About Multimo
					</h4>
					<p>
						<img src="assets/img/prod.png" style="position: absolute; top: 50px;">
					</p>
				</div>
				<div class="col-sm-7">

					<p>
						Starting by making folding chair 517 in 1986, PT. Multi Modern Nusantara  has been developing into a company which specializes in making 100% stainless steel furniture. Recognized by its high-quality product with a brand name “SEAHORSE” for public furniture, it has grown to serve other markets with “Classmate” brand for school furniture and “GoodLiving” brand for Home furniture.
In 2014, in order to bring a new spirit, PT. Multi Modern Nusantara launched an umbrella brand “Multimo” to cover all other brands. Multimo are committed to make more innovation, to improve service and to provide guarantee for all its product so that not only it can serve its customer better but it can also contribute more to a better society.

					</p>


						<h5>Vision</h5>
					<p>
						To become a company recognized for its quality, service, design and value to all its stakeholders in a good and proper way so that it can be part of God’s light to make a better world
					</p>


					<h5>Mission Statement</h5>
					<p>
						To answer to the needs of beautiful, comfortable, easy-maintenance and replacable furniture by focusing on customer-service, customization, after-sales availabilty and after-sales service.
					</p>

						<h5>Mission Goal</h5>
					<p>
						<ul style="color: #777; margin-top: -20px;">
							<li>To excel in providing best product that support human activities </li>
							<li>To do things with a good and proper way</li>
							<li>Be grateful and be optimist</li>
							<li>Always ask God for His direction</li>
						</ul>
					</p>

				</div>
			</div>
		</div>
	</div>


	<div class="section-padding" style="background-image: url('assets/img/content/bg-3.jpg');">
		<div class="container">
			<div class="banner">
				<br>
				<h3>
					Commitment of Value
				</h3>
					<div class="row">
						<div class="col-md-6">
					We commit to give the best value our customer can get with our product. We seek customers' satisfaction and appreciation to the effort we put to create a modern design with good quality but still an economical product.
				</div></div></div>
				<br>
			</div>
		</div>
		<div class="section-overlay" ></div>
	</div>



<br><br>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4>
						Brands
					</h4>
				</div>

				<div class="col-sm-3">
					<figure class="coll-box align-center light" style="background-image: url('assets/img/content/brand-a.png');">
					</figure>
					<p>
						In order to respond customer needs of furniture which is for long-term investment, SEAHORSE comes to answer by providing vast variety of furniture which is functional, sturdy and still economical. Product ranges are available for office, meeting room and function room.
					</p>
				</div>
				<div class="col-sm-3">
					<figure class="coll-box align-center light" style="background-image: url('assets/img/content/brand-c.png');">

					</figure>
					<p>
						Classmate furniture are designed to be students’ best friend in their everyday learning activity. Understanding customer’s need, Classmate provides not only convient but also sturdy and easy-maintained product so that teachers can focus more on teaching activity while feeling secured on students’ safety.

					</p>
				</div>
				<div class="col-sm-3">
					<figure class="coll-box align-center light" style="background-image: url('assets/img/content/brand-b.png');">

					</figure>
					<p>
						Specializing in home furniture, Goodliving is aimed not only for people who are looking for comfort or beauty at the first sight, but also for people who pay attention to long-term beauty, easy-maintenance and comfort.  Goodliving will make our house not only beautiful but also a convenient home for every member of our family. Goodliving, Live a Good Life

					</p>
				</div>
				<div class="col-sm-3">
					<figure class="coll-box align-center light" style="background-image: url('assets/img/content/brand-d.png');">

					</figure>
					<p>
						With the latest technology of UV Print, we give our customer an apportunity to live up their dream through their imagination. Modenta Decorative UV Printing can print on many kinds of flat material like wood, glass, ceramic, leather, steel, stone, etc. Now, start making your imagination come true with Modenta.
					</p>
				</div>

			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
