<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Multimo are committed to make more innovation, to improve service and to provide guarantee for all its product so that not only it can serve its customer better but it can also contribute more to a better society.">
	<meta name="keywords" content="Multimo, Stainless Steel, Chair, Table, Seahorse, Classmate, Good living, Modenta, Furniture,  School Furniture, Office Furniture, Home Furniture, UV Printing">
	<title>MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
		.cover { object-fit: cover; width: auto; height: 281px;}

		html body {
		  background-color: white;
		}

	</style>

</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>


	<section class="slider-area slider-style-2">
				<div class="bend niceties preview-2">
					<div id="ensign-nivoslider" class="slides">
						<img src="assets/img/carousel/1.jpg" alt="">
						<img src="assets/img/carousel/2.jpg" alt="">
						<img src="assets/img/carousel/3.jpg" alt="">
						<img src="assets/img/carousel/4.jpg" alt="">
						<img src="assets/img/carousel/5.jpg" alt="">
					</div>
				</div>
			</section>
			<!-- SLIDER-AREA END -->
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="owl-products products-carousel-02 clearfix">
						<h4>
							Featured
						</h4>
						<div class="carousel-description">
							<div class="text">
								<div class="content">
									<h3>
										Best stuff hand picked products
									</h3>
									<p>
										Check out our <br>
										latest products from <br>
										our Collection.
									</p>
									<a href="by-category.php">
										view all
									</a>
								</div>
							</div>
						</div>
						<ul class="products">
							<li class="product">
								<a href="product-detail.php">
									<img src="assets/img/product/bless.jpg" class="attachment-shop_catalog wp-post-image" alt="">
								</a>
								<div class="product-info">
									<span class="category-product" style="margin-bottom: 20px;">
		                  Chair
									</span>
									<a href="product-detail.php">
										<h3>Seahorse HEART Stainless</h3>
									</a>
								</div>
							</li>
							<li class="product">
								<a href="product-detail.php">
									<img src="assets/img/product/hearth.jpg" class="attachment-shop_catalog wp-post-image" alt="">
								</a>
								<div class="product-info">
									<span class="category-product" style="margin-bottom: 20px;">
		                  Chair
									</span>
									<a href="product-detail.php">
										<h3>Seahorse HEART Stainless</h3>
									</a>
								</div>
							</li>
							<li class="product">
								<a href="product-detail.php">
									<img src="assets/img/product/555.jpg" class="attachment-shop_catalog wp-post-image" alt="">
								</a>
								<div class="product-info">
									<span class="category-product" style="margin-bottom: 20px;">
		                  Chair
									</span>
									<a href="product-detail.php">
										<h3>Seahorse HEART Stainless</h3>
									</a>
								</div>
							</li>
							<li class="product">
								<a href="product-detail.php">
									<img src="assets/img/product/divine.jpg" class="attachment-shop_catalog wp-post-image" alt="">
								</a>
								<div class="product-info">
									<span class="category-product" style="margin-bottom: 20px;">
		                  Chair
									</span>
									<a href="product-detail.php">
										<h3>Seahorse HEART Stainless</h3>
									</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


			<div class="section-padding" style="background-image: url('assets/img/divider/divider.jpg');">
		<div class="container">
			<div class="banner">
				<br>
				<h3>
					Why we are different?
				</h3>
				<a href="about.php">
					find out more
				</a>
				<br>
			</div>
		</div>
		<div class="section-overlay" ></div>
	</div>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="owl-news">
						<h4>
							Gallery
						</h4>
						<div class="news-container">
							<article>
								<figure style="width: 100% ; margin-bottom: 10px;">
									<img src="https://multimo.co.id/assets/img/project/MSCS 1.jpg" alt="">
								</figure>
							</article>
							<article>
								<figure style="width: 100% ; margin-bottom: 10px;">
									<img src="https://multimo.co.id/assets/img/project/Ibis.jpg" alt="">
								</figure>
							</article>
							<article>
								<figure style="width: 100% ; margin-bottom: 10px;">
									<img src="https://multimo.co.id/assets/img/project/gbi-ka.jpg" alt="">
								</figure>
							</article>
							<article>
								<figure style="width: 100% ; margin-bottom: 10px;">
									<img src="https://multimo.co.id/assets/img/project/Shalimar.jpg" alt="">
								</figure>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="single-bg-section" style="background-color:#ccc;">
				<div class="container">
					<div class="newsletter-box">
						<h3>
							Subscribe to our newsletter
						</h3>
						<p>
							Get the latest news and stay informend with our
							latest products and theyr avalability.
						</p>
						<div class="clear"></div>
						<form action="#" method="post" class="newsletter-form">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="you'r email">
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit">subscribe</button>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="owl-news">
						<h4>
							news
						</h4>
						<div class="news-container">
              <article>
                <figure style="width: 100% ; margin-bottom: 10px;">
                  <img src="https://multimo.co.id/assets/img/news/Banner%20Artikel2.jpg" alt="">
                </figure>
                  <span style="text-transform: uppercase; font-weight: 700; font-size: 1.1rem; color: #999;">
										11/06/2018
									</span>

									<div>
                    <a href="https://multimo.co.id/news/62">
											<h2 style="padding: 0;
												font-size: 3rem;
												padding-bottom: 2rem;
												-webkit-transition-delay: 0;
												transition-delay: 0;
												-webkit-transition-property: all;
												-webkit-transition-duration: 0.2s;
												-webkit-transition-timing-function: ease-in-out;
												transition-property: all;
												transition-duration: 0.2s;
												transition-timing-function: ease-in-out;">
  										Tips Membersihkan Kursi Berbahan Kain & Kulit
											</h2>
                    </a>

                  	<a href="https://multimo.co.id/news/62">
											<span style="text-transform: uppercase;
														font-weight: 700;
														font-size: 1.1rem;
														color: #999;
														">
														read article
											</span>
                  </a>
								</div>
              </article>

							<article>
                <figure style="width: 100% ; margin-bottom: 10px;">
                  <img src="https://multimo.co.id/assets/img/news/Banner%20Artikel1.jpg" alt="">
                </figure>
                  <span style="text-transform: uppercase; font-weight: 700; font-size: 1.1rem; color: #999;">
										17/05/2018
									</span>

									<div>
                    <a href="https://multimo.co.id/news/61">
											<h2 style="padding: 0;
												font-size: 3rem;
												padding-bottom: 2rem;
												-webkit-transition-delay: 0;
												transition-delay: 0;
												-webkit-transition-property: all;
												-webkit-transition-duration: 0.2s;
												-webkit-transition-timing-function: ease-in-out;
												transition-property: all;
												transition-duration: 0.2s;
												transition-timing-function: ease-in-out;">
  										Persiapkan Cafemu Jelang Ramadan
											</h2>
                    </a>

                  	<a href="https://multimo.co.id/news/61">
											<span style="text-transform: uppercase;
														font-weight: 700;
														font-size: 1.1rem;
														color: #999;
														">
														read article
											</span>
                  </a>
								</div>
              </article>
							<article>
                <figure style="width: 100% ; margin-bottom: 10px;">
                  <img src="https://multimo.co.id/assets/img/news/Banner%20Artikel.jpg" alt="">
                </figure>
                  <span style="text-transform: uppercase; font-weight: 700; font-size: 1.1rem; color: #999;">
										12 May 2018
									</span>

									<div>
                    <a href="news.php">
											<h2 style="padding: 0;
												font-size: 3rem;
												padding-bottom: 2rem;
												-webkit-transition-delay: 0;
												transition-delay: 0;
												-webkit-transition-property: all;
												-webkit-transition-duration: 0.2s;
												-webkit-transition-timing-function: ease-in-out;
												transition-property: all;
												transition-duration: 0.2s;
												transition-timing-function: ease-in-out;">
  										Relasi Posisi Duduk Dengan Prestasi
											</h2>
                    </a>

                  	<a href="news.php">
											<span style="text-transform: uppercase;
														font-weight: 700;
														font-size: 1.1rem;
														color: #999;
														">
														read article
											</span>
                  </a>
								</div>
              </article>
							<article>
                <figure style="width: 100% ; margin-bottom: 10px;">
                  <img src="https://multimo.co.id/assets/img/news/seahorse%20stainless%20steel%201.jpg" alt="">
                </figure>
                  <span style="text-transform: uppercase; font-weight: 700; font-size: 1.1rem; color: #999;">
										12 May 2018
									</span>

									<div>
                    <a href="news.php">
											<h2 style="padding: 0;
												font-size: 3rem;
												padding-bottom: 2rem;
												-webkit-transition-delay: 0;
												transition-delay: 0;
												-webkit-transition-property: all;
												-webkit-transition-duration: 0.2s;
												-webkit-transition-timing-function: ease-in-out;
												transition-property: all;
												transition-duration: 0.2s;
												transition-timing-function: ease-in-out;">
  										Buktikan Kehebatan Kualitas Produk-Produk SEAHORSE
											</h2>
                    </a>

                  	<a href="news.php">
											<span style="text-transform: uppercase;
														font-weight: 700;
														font-size: 1.1rem;
														color: #999;
														">
														read article
											</span>
                  </a>
								</div>
              </article>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>




    <a href="modenta-uv-printing.php"><img src="assets/img/modenta/modenta-banner.jpg"></a>
    <!--
	<div class="section-padding" style="cursor: pointer; background-image: url('assets/img/modenta/modenta-banner.jpg" onclick="window.location.href='modenta-uv-printing.php'">
		<div class="container">
			<div class="banner" style="visibility: hidden">
				<br>
				<h3>
					Modenta
				</h3>
				<br>
				<a href="category-page">
					find out more<br>ds<br>asd
				</a>
				<br>
			</div>
		</div>
		-->
		<div class="section-overlay" ></div>
	</div>
<script src="assets/js/vendor/jquery-1.12.0.min.js"></script>
<script src="assets/lib/js/jquery.nivo.slider.js"></script>
<script src="assets/lib/home.js"></script>
	<?php include ('footer.php') ?>
