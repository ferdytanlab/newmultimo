<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Product details for Seahorse HEART Stainless">
	<meta name="keywords" content="Seahorse HEART Stainless, <?php echo $productDesc;?>">
	<title>Seahorse HEART Stainless - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}

	.materialmul span{
	text-transform: uppercase;
	font-size: 1.2rem;
	font-weight: 700;
	color: #555;
	margin-bottom: 1rem;
	display: block;
	}

	.mb-30 {
		margin-bottom: 30px;
	}
	.mr-5 {
		margin-right: 5px;
	}

	</style>
</head>
<body class="fixed-header">

	<!--<div class="product-pup-up"></div>-->

	<?php include ('header.php') ?>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="mini-breadcrumb" style="visibility: hidden">
						<ol>
							<li>
								<a href="#">
									Home
								</a>
							</li>
							<li>
								<a href="#">
									Furniture
								</a>
							</li>
							<li>
								<h1>Chair</h1>
							</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="product">
						<div class="images">
							<a href="assets/img/product/Hearth.jpg" class="woocommerce-main-image zoom lightbox" title="">
								<img src="assets/img/product/Hearth.jpg" class="attachment-shop_single wp-post-image" alt="" title="Seahorse HEART Stainless">
							</a>
							<div class="thumbnails columns-4">
								<a href="assets/img/product/Hearth.jpg" class="zoom first lightbox" title="">
									<img src="assets/img/product/Hearth.jpg" class="attachment-shop_thumbnail" alt="">
								</a>
								<a href="assets/img/product/Hearth Biru Right.jpg" class="zoom first lightbox" title="">
									<img src="assets/img/product/Hearth Biru Right.jpg" class="attachment-shop_thumbnail" alt="">
								</a>
                <a href="assets/img/product/HearthBiruBack.jpg" class="zoom first lightbox" title="">
                  <img src="assets/img/product/HearthBiruBack.jpg" class="attachment-shop_thumbnail" alt="">
                </a>
                <a href="assets/img/product/HearthBiruTumpuk.jpg" class="zoom first lightbox" title="">
									<img src="assets/img/product/HearthBiruTumpuk.jpg" class="attachment-shop_thumbnail" alt="">
								</a>


							</div>
						</div>
						<div class="summary entry-summary">
							<h1 class="product_title entry-title">
								Seahorse HEART Stainless
								<span>

								</span>
							</h1>

							<div class="single-product-color-select">
								<span>
									<i class="far fa-circle mr-5"></i>  Available on<br>
								</span>
								<a href="https://www.lazada.co.id/shop/multimo/?spm=a2o4j.pdp.seller.1.28034589x8d534" target="_blank">
									<img src="assets/img/market/lazada.png" style="height:32px; width:auto; margin-right:10px;">
								</a>

								<a href="https://www.tokopedia.com/furo/kursi-banquet-reborn-seahorse-furo?trkid=f=Ca0000L000P0W0S0Sh00Co0Po0Fr0Cb0_src=other-product_page=1_ob=32_q=_po=5_catid=1043&src=other" target="_blank">
									<img src="assets/img/market/tokopedia.png" style="height:32px; width:auto; margin-right:10px; ">
								</a>

								<a href="https://www.bukalapak.com/p/perlengkapan-kantor/alat-kantor/furniture-kantor/g49hdo-jual-kursi-susun-sea-horse-heart-stainless-oscar-putih-kursi-meeting-free-ongkir-jawa-bali?from=&product_owner=normal_sellera" target="_blank">
									<img src="assets/img/market/bukalapak.png" style="height:32px; width:auto; margin-right:10px;">
								</a>
							</div>
							<!--
							<div class="single-post-social">

								<a href="http://twitter.com/intent/tweet?source=webclient&text=http://www.multimo.co.id/product-detail.php" target="_blank" class="social-button">
									<i class="far fa-share-square"></i>
									<span class="button-text">
										twitter
									</span>
								</a>

								<a href="http://www.facebook.com/sharer.php?s=100&u=http://www.multimo.co.id/product-detail.php" target="_blank" class="social-button">
									<i class="far fa-share-square"></i>
									<span class="button-text">
										facebook
									</span>
								</a>

							</div>
							-->
							<div class="single-product-description">
								<p>
									Kursi Heart sangat cocok untuk semua kegiatan Anda. Kursi ini memiliki sandaran punggung dengan motif jala sehingga sangat nyaman untuk penggunaan yang cukup lama.
								</p>

							</div>



							<div class="materialmul">
									<span>
										<i class="far fa-circle mr-5"></i>  pipe materials
									</span>
                  <div class="mb-30">
                    Stainless Steel
                  </div>
              </div>

              <div class="materialmul">
									<span>
										<i class="far fa-circle mr-5"></i>   cover materials
									</span>
                  <div>
                    Fabric
                  </div>
                  <a href="assets/img/product/Heart01.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F01.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
                  <a href="assets/img/product/Heart02.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F02.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
                  <a href="assets/img/product/Heart03.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F03.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
                  <a href="assets/img/product/Heart04.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F04.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
									<a href="assets/img/product/hearth.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F05.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
                  <a href="assets/img/product/Heart06.jpg" class="woocommerce-main-image zoom lightbox" title="">
                    <img src="assets/img/cover/F06.jpg" class="attachment-shop_single wp-post-image" width="20%" style="padding-bottom: 8px;" alt="" title="Seahorse HEART Stainless">
                  </a>
              </div>

              <div class="single-product-color-select">
									<span>
										<i class="far fa-circle mr-5"></i> Dealer Information
									</span>
                  <div class="available-colors-list">
                    <p>Harga : 327.500 ( nett )</p>
										<a href="assets/img/product/wire1.jpg" target="_blank" class="social-button">
											<i class="far fa-share-square"></i>
												<span class="button-text">
													specification
												</span>
										</a>

                  </div>


              </div>


							</form>

						</div>
						<div class="clearfix" style="margin-bottom:35px;"></div>
						<div class="row">
							<h4>
								Features
							</h4>
							<br>
							<iframe width="640" height="360" src="https://www.youtube.com/embed/IO2M3cBj8-s" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
							<img src="assets/img/product/fea1.jpg">
							<img src="assets/img/product/fea2.jpg">
						</div>
						<div class="clearfix" style="margin-bottom:35px;"></div>
						<div class="row">
							<h4>
								Recommendation
							</h4>
							<br>
							<div class="col-sm-12 multiproduct">
			          <div class="col-sm-3 col-xs-6 multimo-product">
			           <a href="product-detail.php">
			             <img src="assets/img/product/Hearth.jpg" width="215" height="215" class="attachment-shop_catalog wp-post-image" alt="Seahorse HEART Stainless">
			           </a>

			           <div class="multimo-info">
			             <span class="brand-product">
			               <span class="the-product-status" onclick="window.location.href='product-detail.php'" style="cursor:pointer;">
			                 <img align="middle" src="assets/img/brand/seahorse.png" width="75%">
			               </span>
			             </span>
			             <a href="product-detail.php">
			               <h3>Seahorse HEART Stainless</h3>
			             </a>
			           </div>
			          </div>

			          <div class="col-sm-3 col-xs-6 multimo-product">
			           <a href="product-detail.php">
			             <img src="assets/img/product/Hearth.jpg" width="215" height="215" class="attachment-shop_catalog wp-post-image" alt="Seahorse HEART Stainless">
			           </a>

			           <div class="multimo-info">
			             <span class="brand-product">
			               <span class="the-product-status" onclick="window.location.href='product-detail.php'" style="cursor:pointer;">
			                 <img align="middle" src="assets/img/brand/seahorse.png" width="75%">
			               </span>
			             </span>
			             <a href="product-detail.php">
			               <h3>Seahorse HEART Stainless</h3>
			             </a>
			           </div>
			          </div>

			          <div class="col-sm-3 col-xs-6 multimo-product">
			           <a href="product-detail.php">
			             <img src="assets/img/product/Hearth.jpg" width="215" height="215" class="attachment-shop_catalog wp-post-image" alt="Seahorse HEART Stainless">
			           </a>

			           <div class="multimo-info">
			             <span class="brand-product">
			               <span class="the-product-status" onclick="window.location.href='product-detail.php'" style="cursor:pointer;">
			                 <img align="middle" src="assets/img/brand/seahorse.png" width="75%">
			               </span>
			             </span>
			             <a href="product-detail.php">
			               <h3>Seahorse HEART Stainless</h3>
			             </a>
			           </div>
			          </div>

								<div class="col-sm-3 col-xs-6 multimo-product">
			           <a href="product-detail.php">
			             <img src="assets/img/product/Hearth.jpg" width="215" height="215" class="attachment-shop_catalog wp-post-image" alt="Seahorse HEART Stainless">
			           </a>

			           <div class="multimo-info">
			             <span class="brand-product">
			               <span class="the-product-status" onclick="window.location.href='product-detail.php'" style="cursor:pointer;">
			                 <img align="middle" src="assets/img/brand/seahorse.png" width="75%">
			               </span>
			             </span>
			             <a href="product-detail.php">
			               <h3>Seahorse HEART Stainless</h3>
			             </a>
			           </div>
			          </div>
			         </div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="comment-container">
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="comments-num">
										<div class="container">
											<h1 class="title-comments">
												<span>0</span> Comments

											</h1>
										</div>
									</div>
								</div>
								<!-- ============== COMMENTS ============= -
								<div class="container">
									<ul class="comments">
                    <li>

								        <div class="pingback even thread-even depth-1">
                            <div class="left-section">
                                <img alt="" src="assets/img/content/author-avatar.jpg">
                            </div>
                            <div class="right-section" style="min-height: 130px;">
                                <h1>
                                    Nama
                                </h1>

                                <div class="comment-text">
                                    <p>
                                       Testimoni
                                    </p>
                                </div>

                            </div>
                        </div>
                    </li>
									</ul>

									</div>
								</div>
							-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php include ('footer.php') ?>
