<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="We share news, tips and trick, and how to, to your furniture">
	<meta name="keywords" content="Bagaimana, Cara, Tips, Furniture, How to">
	<title>NEWS AND TIPS - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="articles-area">
						<div class="row">
							<div class="col-sm-12">
								<article>
										<header>
												<h2 style="font-size: 3.2rem">
														<a href="news.php">
																Title 1
														</a>
												</h2>
												<div class="post-details">
														Posted by
														<a href="#">
																Multimo
														</a>
														on 12 May 2018
												</div>
										</header>
										<figure>
												<a href="news.php">
														<img src="assets/img/news/PameranSemarang.jpg" alt="">
												</a>
												<figcaption>
														<p>
																Lorem Ipsum
														</p>
												</figcaption>
										</figure>
										<footer>
												<a href="news.php" class="btn btn-primary">Read post</a>
										</footer>
								</article>

							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<nav class="the-pagination clearfix">
									<ul class="pagination clearfix">
											<li class="active">
													<a href="news-list.php">
															1
													</a>
											</li>

									</ul>
									<ul class="pager clearfix">
										<li>
												<a href="news-list.php>">
														<i class="fa fa-chevron-left"></i>
												</a>
										</li>
										<li>
												<a href="news-list.php">
														<i class="fa fa-chevron-right"></i>
												</a>
										</li>

									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="sidebar">
						<div class="row">
							<div class="col-sm-12">
								<aside class="widget widget_categories">
									<h3>News Feed</h3>
									<hr>
									<div style="font-size: 11px">12 May 2018</div>
									<div>newsTitle</div>
									<hr>
								</aside>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<aside class="widget widget_flickr" style="visibility: hidden;">
									<h3>text</h3>
									<div class="textwidget">
										<p>
											Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate ele ifend tellus. Aenean leo ligula, por ttitor eu, consequat vitae.
										</p>
									</div>
								</aside>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
