<?php
?>
<style>
html body {
  background-color: white;
}
</style>

<link rel="shortcut icon" type="image/png" href="assets/img/logoico.png"/>

<link rel="stylesheet" href="assets/css/master.css">
<link rel="stylesheet" href="assets/css/product.css">
<link rel="stylesheet" href="assets/css/productdetail.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.css">
<link rel="stylesheet" href="assets/css/15.css">
<link rel="stylesheet" href="assets/css/dzsparallaxer.scss">

<link rel="stylesheet" href="assets/lib/css/nivo-slider.css">
<link rel="stylesheet" href="assets/lib/css/preview.css">


<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="4a77e4c6-1d52-4515-b15b-379d1374d6b8";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-77097361-1', 'auto');
ga('send', 'pageview');

</script>
