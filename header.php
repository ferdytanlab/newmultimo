<?php

?>

<header class="main-header mega-header">
	<div class="container clearfix">
		<div class="menu-overlay-for-body"></div>
		<div class="logo-area">
			<a class="phone-menu-trigger" href="index.html#" aria-haspopup="true">
				<i class="fa fa-bars"></i>
			</a>
			<a href="index.html">
				<img src="assets/img/header/logo.png" alt="">
			</a>
		</div>
		<div class="header-user-options">
			<div class="user-sign-in">
					<i class="fas fa-question-circle"></i>
					<a href="https://multimo.co.id/faq"><span class="text-header-span">FAQ</span></a>
				</div>
		</div>
		<div class="header-user-info">
				<div class="info">
					<i class="fa fa-phone"></i>
					<a href="tel:+62318544449">+62 31 85 4444 9</a>
				</div>
			</div>
		<div class="clearfix"></div>
		<div class="menu-container clearfix">
			<div class="additional-buttons">
				<form class="main-menu-search-form clearfix" method="get" action="search-result">
					<div class="stylish-big-search-form">
						<input type="search" id="search" class="main-search-input" placeholder="Search for" name="search" autofocus  onKeyPress="return submitenter(this,event)"/>
					</div>
					<button type="submit" class="search-trigger">
						<i class="fa fa-search"></i>
					</button>
				</form>

				<div class="my-cart-link" aria-haspopup="true">
					<a>
						<i class="fas fa-user-lock"></i>
					</a>
					<div class="header-cart-container">
						<div class="header-cart" style="padding:10px;">

							<div class="cart-head clearfix">
								<p>
									Store Login
								</p>
								<p class="prod-num" style="visibility:hidden;">
									Login
								</p>
							</div>

							<div class="cart-entries">

							<form class="form" method="post">
								<table>
							    <tbody>
										<tr>
							    		<td class="name">
							    			<label for="name" style="font-size:13px;">Username</label><br>
													<input type="text" name="bname" id="bname" class="form-control">
							    		</td>
							    	</tr>
										<tr>
							    		<td class="name">
							    			<label for="name" style="font-size:13px;">Password</label><br>
													<input type="password" name="name" id="name" class="form-control">
							    		</td>
							    	</tr>
							    	<tr>
							    		<td class="submit">
							    		<br>
							    			<input id="submit" name="submit" type="submit" value="Sign In">
							    		</td>
							    	</tr>
									</tbody>
								</table>
				      </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<nav class="menu-area">
				<div class="phone-logo">
					<a href="index.php">
						<img src="assets/img/header/logo.png" alt="" width="169" height="22">
					</a>
				</div>
				<ul class="clearfix">
					<li>
						<a href="index.php">
							home
						</a>
					</li>
					<li>
						<a href="about.php">
							About
						</a>

					</li>
					<li>
						<a href="product-gallery.php">
							Product Gallery
						</a>

					</li>
					<li>
						<a href="#">
							Product
						</a>

						<div class="main-menu-dropdown big-mega-menu light">
							<div class="mega-menu-container clearfix">
								<div class="col-sm-7 header-col">
									<div class="row">
										<div class="col-sm-4 header-col">
											<ul>
												<li>
													<a href="#">
														By Category
													</a>
													<ul>
														<li>
																<a href="by-category.php">
																		Office
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		Home
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		School
																</a>
														</li>
														<li>
																<a href="chair.php">
																		Chair
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		Table
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		Restaurant
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		Custom Project
																</a>
														</li>
														<li>
																<a href="by-category.php">
																		Wall Decoration
																</a>
														</li>
													</ul>
												</li>
											</ul>
										</div>
										<div class="col-sm-4 header-col">
											<ul>
												<li>
													<a href="#">
														By Brand
													</a>
													<ul>
														<li>
																<a href="seahorse.php">
																		<img src="assets/img/brand/seahorse.png" height="60">
																</a>
														</li>
														<li>
																<a href="by-brand.php">
																		<img src="assets/img/brand/classmate.png" height="60">
																</a>
														</li>
														<li>
																<a href="by-brand.php">
																		<img src="assets/img/brand/goodliving.png" height="60">
																</a>
														</li>
														<li>
																<a href="by-brand.php">
																		<img src="assets/img/brand/modenta.png" height="60">
																</a>
														</li>
													</ul>
												</li>
												<li style="visibility: hidden">
													<a href="#">
														Furniture
													</a>
													<ul>
														<li>
															<a href="#">
																Furniture Item
															</a>
														</li>
													</ul>
												</li>
											</ul>
										</div>
										<div class="col-sm-4 header-col">
											<ul>
												<li>
													<a href="#">
														Media
													</a>

													<ul>
														<li>
															<a href="assets/media/Promo-Cash-Back.jpg" >
																	Modenta UV Print 2016
															</a>
															<a href="assets/media/Promo-Cash-Back.jpg" >
																	Multimo Funtastic Promo
															</a>
															<br>
															<a href="assets/media/Promo-Cash-Back.jpg">
																	Promo Cash Back
															</a>
															<br>
														</li>
													</ul>
												</li>
											</ul>
										</div>

									</div>
								</div>
								<div class="col-sm-5 header-col">
									<div class="owl-collesction-header right">
										<div class="menu-collection-box style-03" style="background-image: url('assets/img/product/Blueberry 1.jpg');"></div>
									</div>
								</div>
							</div>
						</div>

					</li>
					<li>
						<a href="news-list.php">
							News and Tips
						</a>
					</li>

					<li>
					<a href="warranty-submit.php">Warranty Submit</a>
					</li>

					<li>
						<a href="#">
							Contact
						</a>
						<ul>
							<li>
							<a href="contact.php">Contact</a>
							</li>
							<li>
							<a href="request-sample.php">Request Sample</a>
							</li>

							<li>
							<a href="opportunity.php">Business Opportunity</a>
							</li>
							<li>
							<a href="job-vacancy.php">Job Vacancy</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="social-icons-phone">
					<li>
						<a href="https://www.instagram.com/multimo_furniture/">
							<i class="fa fa-instagram"></i>
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/MultimoFurniture/">
							<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/MultimoRd2">
							<i class="fa fa-twitter"></i>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>

	<SCRIPT TYPE="text/javascript">
		 /*function submitenter(myfield,e)
		 {
		 	var keycode;
			if (window.event)
				keycode = window.event.keyCode;
			else if (e)
				keycode = e.which;
			else
				return true;
			if (keycode == 13)
			{
				myfield.form.submit();
				return false;
			}
			else
				return true;
		} //--> */
		 function submitenter(myfield,e)
		 {
			 var keycode;
			 if (window.event)
				 keycode = window.event.keyCode;
			 else if (e)
				 keycode = e.which;
			 else
				 return true;

			 if (keycode == 13) {
				 $search = $('#search').val()
				window.location.replace(" search-result/" + $search);

			 }
		 } //-->

		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete?");
		  if (x)
			  return true;
		  else
			return false;
		}
        </SCRIPT>
