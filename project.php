<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="description" content="Multimo Projects. We work on the best projects for the best customer and clients."
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="keyword" content="Decoration, Interior design, Furniture, Banquet Chair, Buffet Table, School Table, School Chair, Café Table, Home, Interior, Dining Table, Room, Design , Steel, Meeting Table, room, Hall, facilities, Event, Celebrate, business, kursi tamu, kursi kantor,kursi susun,kursi susun, surabaya, kursi susun stainless, spesifikasi kursi, susun, kursi stainless, furniture lengkap dan murah, kursi stainless steel, kursi makan stainless, kursi tunggu stainless, kursi restoran, Harga Kursi, kursi lipat, kursi lipat portable, kursi gereja, kursi hotel, kursi stainless, bangku sekolah, bangku kuliah, furniture stainless steel, kursi elephant, meja makan murah Surabaya, meja makan kayu, meja makan kaca">
	<title>PROJECT GALLERY - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
						<figure class="collection-box align-center light" style="height: 163px; background-image: url('assets/img/project/1Brighton.jpg');">						<figcaption>
										<p>
												2018
										</p>
										<h3>
												projectName
										</h3>
										<a href="project-gallery.php" class="light">
												view projects
										</a>
								</figcaption>

						</figure>
				</div>
			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
