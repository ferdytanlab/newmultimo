<?php
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="We want you to succeed and we will help you make it happen. We offer you comprehensive support in order for you to provide your customers a truly unique and high-quality solution to their need.">
	<meta name="keywords" content="request sample, Surabaya, Sidoarjo, Opportunity, Job Vacancy, Furniture, Distributor,  Kerja sama, Join, Multimo, facilities">
	<title>Warranty Submit - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>

	<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h3>
						Warranty Submit Form
						<img src="assets/img/content/under.jpg">
					</h3>
				</div>

				<div class="col-sm-12">
<!--
					<div class="cognito">
<script src="https://services.cognitoforms.com/s/VhRZ-HW_tUq1JU0qcy6NXA"></script>
<script>Cognito.load("forms", { id: "1" });</script>
</div>

-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function (e){
$("#frmContact").on('submit',(function(e){
	e.preventDefault();
	$('#loader-icon').show();
	var valid;
	valid = validateContact();
	if(valid) {
		$.ajax({
		url: "contact_mail.php",
		type: "POST",
		data:  new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
		$("#mail-status").html(data);
		$('#loader-icon').hide();
		},
		error: function(){}

		});
	}
}));

function validateContact() {
	var valid = true;
	$(".demoInputBox").css('background-color','');
	$(".info").html('');

	if(!$("#userName").val()) {
		$("#userName-info").html("(required)");
		$("#userName").css('background-color','#FFFFDF');
		valid = false;
	}
	if(!$("#userEmail").val()) {
		$("#userEmail-info").html("(required)");
		$("#userEmail").css('background-color','#FFFFDF');
		valid = false;
	}
	if(!$("#userEmail").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
		$("#userEmail-info").html("(invalid)");
		$("#userEmail").css('background-color','#FFFFDF');
		valid = false;
	}
	if(!$("#subject").val()) {
		$("#subject-info").html("(required)");
		$("#subject").css('background-color','#FFFFDF');
		valid = false;
	}
	if(!$("#content").val()) {
		$("#content-info").html("(required)");
		$("#content").css('background-color','#FFFFDF');
		valid = false;
	}

	return valid;
}

});
</script>
<form id="frmContact" action="" method="post">
<div id="mail-status"></div>
<div>
<label style="padding-top:20px;">Name</label>
<span id="userName-info" class="info"></span><br/>
<input type="text" name="userName" id="userName" class="demoInputBox">
</div>
<div>
<label>Email</label>
<span id="userEmail-info" class="info"></span><br/>
<input type="text" name="userEmail" id="userEmail" class="demoInputBox">
</div>
<div>
<label>Product that you buy</label>
<span id="subject-info" class="info"></span><br/>
<input type="text" name="subject" id="subject" class="demoInputBox">
</div>
<div>
<label>Content</label>
<span id="content-info" class="info"></span><br/>
<textarea name="content" id="content" class="demoInputBox" cols="60" rows="6"
placeholder="
Name:
Email:
Address:
Phone:
"
></textarea>
</div>
<div>
<label>Attachment</label><br/>
<input type="file" name="attachmentFile" id="attachmentFile" class="demoInputBox">
</div>
<div>
<input type="submit" value="Send" class="btnAction" />
</div>
</form>

		<div id="mail-status"></div>



				</div>


			</div>
		</div>
	</div>



	<?php include ('footer.php') ?>
