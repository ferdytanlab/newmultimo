<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="newsTitle">
	<meta name="keywords" content="Bagaimana, Cara, Tips, Furniture, How to">
	<title>newsTitle - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

    <!--	<div class="product-pup-up"></div> -->

	<?php include ('header.php') ?>
	<img src="assets/img/news/PameranSemarang.jpg">
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="single-article">
						<div class="row">
							<div class="col-sm-12">
								<article>
									<header>
										<h2 style="font-size: 3.2rem; line-height: 50px;">
											<a href="#">
												newsTitle
											</a>
										</h2>
										<div class="post-details">
											Posted by
											<a href="#">
												MULTIMO
											</a>
											on 12 May 2018
										</div>
										<hr>
									</header>
									<div class="article-entries">
										Lorem Ipsum
									</div>
								</article>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="single-post-social">
									<a href="http://twitter.com/intent/tweet?source=webclient&text=http://www.multimo.co.id/news.php" target="_blank" class="social-button">
									<i class="fa fa-share-square-o"></i>
									<span class="button-text">
										twitter
									</span>
								</a>

								<a href="http://www.facebook.com/sharer.php?s=100&u=http://www.multimo.co.id/news.php?id target="_blank" class="social-button">
									<i class="fa fa-share-square-o"></i>
									<span class="button-text">
										facebook
									</span>
								</a>
								</div>
							</div>
						</div><!--
						<div class="row">
							<div class="col-sm-12">
								<div class="author-info">
									<div class="left-author-info">
										<figure>
											<img src="assets/img/logoico.png" alt="">
										</figure>
									</div>

									<div class="right-author-info">
										<h2>
											About the Author: <span>Multimo</span>
										</h2>
										<p>Integer fringilla erat et tellus ultrices, et dictum lacus laoreet. Donec in purus orci. Morbi tincidunt orci congue, blandit nisl eget, suscipit nulla. Nam feugiat ullamcorper est ut euismod. Sed pretium orci sed ipsum imperdiet, id ultrices turpis laoreet. </p>
									</div>

								</div>
							</div>
						</div>-->
					</div>
				</div>
				<div class="col-sm-3">
					<div class="sidebar">
						<div class="row">
							<div class="col-sm-12">
								<aside class="widget widget_categories">
									<h3>News Feed</h3>
									<hr>
									<div style="font-size: 11px">12 May 2018</div>
									<a href="news.php">
									<div>newsTitle</div>
									</a>
									<hr>

								</aside>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<aside class="widget widget_flickr" style="visibility: hidden;">
									<h3>text</h3>
									<div class="textwidget">
										<p>
											Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate ele ifend tellus. Aenean leo ligula, por ttitor eu, consequat vitae.
										</p>
									</div>
								</aside>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>



	<?php include ('footer.php') ?>
