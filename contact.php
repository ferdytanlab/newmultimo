<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="Don't hestitate to contact us, get in touch, and get direction.">
	<meta name="keywords" content="Location, Surabaya, Gedangan, Sidoarjo, East Java, Jawa Timur, Get in touch, Direction, Contact">
	<title>MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDy5czU_R4bjrSUVmdiRnEq40SGboKCvv0&callback=initMap"
  type="text/javascript"></script>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>
	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="mini-info-box">
						<h3>
							Office
						</h3>
						<p>
							Jl. Muncul 10, Gedangan<br>
							Sidoarjo 61254 Jawa Timur
						</p>
						<a href="https://www.google.co.id/maps/dir//Jalan+Muncul+No.10,+Gedangan,+Sidoarjo,+Jawa+Timur+61254/@-7.3843581,112.7208658,17z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x2dd7e4869a260051:0xd5072e2f7340d24b!2m2!1d112.7230545!2d-7.3843634">
							GET DIRECTION
							<i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="mini-info-box">
						<h3>
							Sales Representative
						</h3>
						<p>
							For information about an order or about <br>
							our products contact us at
						</p>
						<a href="mailto:project@multimo.co.id">
							project@multimo.co.id
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="mini-info-box">
						<h3>
							Give us a call
						</h3>
						<p>
							Need more info but mail takes time? <br>
							Use your phone is lighting fast!
						</p>
						<a class="phone-info">
							<i class="fa fa-phone"></i>
							<a href="tel:+62318544449">+62 31 85 4444 9</a>
						</a>
						<br>
						<a class="phone-info">
							<i class="fa fa-phone"></i>
							<a href="tel:+62318544572"> +62 31 85 4457 2</a>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="mini-info-box">
						<h3>
							Live Chat
						</h3>
						<p style="height: 85px;">
							For fast respon, we can chat with these platform below<br>
							<a href="https://api.whatsapp.com/send?phone=6283849208080&text=Hi%20Nama%20Saya" target="_blank"><img src="assets/img/wa.png"></a>

							<img src="assets/img/livechat.png">

						</p>
						<!--<a class="phone-info">
							<i class="fa fa-phone"></i>
							<a href="tel:+62318544449">+62 31 85 4444 9</a> /
							<a href="tel:+62318544572">+62 31 85 4457 2</a>
						</a>-->
					</div>
				</div>
				<div class="col-sm-12">
					<div id="map-canvas" class="google-map" data-lat="-7.384497" data-long="112.723076" data-img="assets/img/contact/marker.png"></div>
					<br>
					<br>
				</div>

				<div class="col-sm-12" id="stockist">
					<h3>Stockist</h3>
				</div>

				<div class="col-sm-4">
					<table class="shop_table cart after-checkout">
						<thead>
							<tr>
								<th class="product-name" style="text-align: left">Region</th>
							</tr>
						</thead>
						<tbody>
							<tr class="cart_table_item">
									<!-- The thumbnail -->
									<td class="product-thumbnail">
											<div class="product-info">
													<a class="product-title" href="javascript:showCity">
															regionName
													</a>
											</div>
									</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="col-sm-4">
                    <div id="viewCity"></div>

				</div>

				<div class="col-sm-4">
                    <div id="viewStore"></div>

				</div>

			</div>
		</div>
	</div>

	<?php include ('footer.php') ?>
	<script src="assets/js/map.js"></script>


	<!--[if lte IE 9 ]>
		<script src="assets/js/placeholder.js"></script>
		<script>
			jQuery(function() {
				jQuery('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

<script type="text/javascript" language="JavaScript"><!--
function HideContent(d) {
document.getElementById(d).style.display = "none";
}
function ShowContent(d) {
document.getElementById(d).style.display = "block";
}
function ReverseDisplay(d) {
if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "block"; }
else { document.getElementById(d).style.display = "none"; }
}
//-->

    $(document).ready(function(){
        showCity(0);
        showStore(0);
    });
function showCity(id)
{
    $.ajax({ //make ajax request to cart_process.php
        url: "showContact.php",
        type: "POST",
        data: { action: "showCity",id:id},
        success: function (data) { //on Ajax success
            console.log(data);

            //$('#city tbody').append(data);
            $("#viewCity").html(data);


        }//,
         //error: function(request, textStatus, errorThrown){
        //    alert(request.responseText);
        //alert("ddd");

        //  }
    });
}
    function showStore(id)
    {        $.ajax({ //make ajax request to cart_process.php
            url: "showContact.php",
            type: "POST",
            data: { action: "showStore",id:id},
            success: function (data) { //on Ajax success
                console.log(data);

                //$('#city tbody').append(data);
                $("#viewStore").html(data);


            }//,
             //error: function(request, textStatus, errorThrown){
            //    alert(request.responseText);
            //alert("ddd");

            //  }
        });
    }
</script>


	<!-- ================================================== -->
	<!-- =============== END JQUERY SCRIPTS ================ -->
	<!-- ================================================== -->

</body>
</html>
