<?php

?>

<div class="second-footer">
		<div class="container">
			<div class="row">
				<aside class="col-sm-4 widget widget_nav_menu">

					<h3>Information</h3>
					<ul class="menu">
						<li class="menu-item current-menu-item">
							<a href="about.php">About Us</a>
						</li>
						<li class="menu-item current-menu-item">
							<a href="contact.php">Contact Us</a>
						</li>
						<li class="menu-item current-menu-item">
							<a href="testimonial.php">Testimonial</a>
						</li>
						<li class="menu-item menu-item-has-children">
							<a href="faq.php">Frequently Ask Questions (F.A.Q)</a>
						</li>
						<li class="menu-item current-menu-item">
							<a href="sLogin.php">Store Login</a>
						</li>
					</ul>
				</aside>

				<aside class="col-sm-3 col-sm-offset-1  widget widget_text">
					<h3>Follow Us</h3>

					<ul class="footer-social">
				<li>
					<a href="https://www.instagram.com/multimo_furniture/">
						<i class="fab fa-instagram"></i>
					</a>
				</li>
				<li>
					<a href="https://www.facebook.com/MultimoFurniture">
						<i class="fab fa-facebook"></i>
					</a>
				</li>
				<li>
					<a href="https://twitter.com/MultimoRd2">
						<i class="fab fa-twitter"></i>
					</a>
				</li>
			</ul>


				</aside>
				<!--
				<aside class="col-sm-3 col-sm-offset-1  widget widget_text">
					<h3>contact informations</h3>
					<div class="textwidget">
						<div class="row">
							<div class="col-sm-1">
								<h5 style="color: #ccc"><i class="fa fa-home"></i></h5>

							</div>
							<div class="col-sm-10">
								<h5 style="color: #ccc">Address :</h5>
									<p>Jl. Muncul 10, Gedangan<br>
										Sidoarjo 61254 Jawa Timur</p>
							</div>

							<div class="col-sm-1">
								<h5 style="color: #ccc"><i class="fa fa-phone"></i></h5>

							</div>
							<div class="col-sm-10">
								<h5 style="color: #ccc">Phone :</h5>
									<p><a href="tel:+62318544449" style="color: #999;">+62 31 85 4444 9 </a><br>
								<a href="tel:+62318544572" style="color: #999;">+62 31 85 4457 2</p>
							</div>

							<div class="col-sm-1">
								<h5 style="color: #ccc"><i class="fa fa-envelope"></i></h5>

							</div>
							<div class="col-sm-10">
								<h5 style="color: #ccc">Email :</h5>
									<p><a href="mailto:project@multimo.co.id" style="color: #999">project@multimo.co.id</a></p>
							</div>

							<div class="col-sm-1">
								<h5 style="color: #ccc"><i class="fa fa-clock-o"></i></h5>

							</div>
							<div class="col-sm-10">
								<h5 style="color: #ccc">Office Hour :</h5>
									<p><span>Mon - Fri :</span><span> 7:30am - 5:30pm</span></p>
							</div>

						</div>
						<a href="contact.php" class="btn btn-primary" style="background-color: #aaa">get in touch</a>
					</div>
				</aside>
				-->
			</div>
		</div>
	</div>

	<footer class="main-footer">
		<div class="container">
			<div class="logo">
				<a href="index.php">
					<span style="padding: 10px;"><img src="assets/img/header/seahorse.png" alt="seahorse" width="59" height="36"></span>
					<span style="padding: 10px;"><img src="assets/img/header/classmate.png" alt="classmate" width="72" height="28"></span>
					<span style="padding: 10px;"><img src="assets/img/header/good.png" alt="goodliving" width="43" height="36"></span>
					<span style="padding: 10px;"><img src="assets/img/header/modenta.png" alt="modenta" width="59" height="36"></span>
				</a>
			</div>
			<nav class="footer-menu">
				<ul>
					<li>
						<a href="privacy.php">
							Privacy & Cookies
						</a>
					</li>
					<li>
						<a href="term-condition.php">
							Terms & Conditions
						</a>
					</li>
					<li>
						<a href="accessibility.php">
							Accessibility
						</a>
					</li>
				</ul>
			</nav>
			<p class="copyrights">
				© <?php echo date("Y"); ?>. PT. MULTI MODERN NUSANTARA. ALL RIGHTS RESERVED.
			</p>
		</div>
	</footer>

	<!-- ================================================== -->
	<!-- =============== START JQUERY SCRIPTS ================ -->
	<!-- ================================================== -->

	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/plugins.js"></script>
	<script src="assets/js/main.min.js"></script>
	<script src="assets/js/custom.js"></script>


	<!--[if lte IE 9 ]>
		<script src="assets/js/placeholder.js"></script>
		<script>
			jQuery(function() {
				jQuery('input, textarea').placeholder();
			});
		</script>
	<![endif]-->

	<!-- ================================================== -->
	<!-- =============== END JQUERY SCRIPTS ================ -->
	<!-- ================================================== -->

</body>
</html>
