<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Detail Bahan Baku</div>
                <div class="card-body">
                  <form action="pembelian.php" data-parsley-validate="" novalidate="">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="inputMerk">Merk</label>
                        <span class="col-12 col-sm-8 col-lg-6">
                          <select class="select2">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                          </select>
                        </span>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label for="inputProduk">Nama Item</label>
                        <span class="col-12 col-sm-8 col-lg-6">
                          <select class="select2">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                          </select>
                        </span>
                      </div>
                    </div>
                  </div>

                    <table class="table">
                      <thead>
                        <tr>
                          <th style="width:5%;">Jumlah</th>
                          <th style="width:20%;">Satuan</th>
                          <th style="width:30%;">Harga</th>
                          <th style="width:50%;">Subtotal</th>
                          <th class="actions"></th>
                        </tr>
                      </thead>
                      <tbody class="no-border-x">
                        <tr>
                          <td>
                            <span class="col-12 col-sm-8 col-lg-6">
                              <input id="inputJumlah" type="tel" placeholder="" class="form-control">
                            </span>
                          </td>
                          <td>
                            <span class="col-12 col-sm-8 col-lg-6">
                              <select class="form-control">
                                <option value="Kg" selected>Kg</option>
                                <option value="Box">Box</option>
                                <option value="Pcs">Pcs</option>
                              </select>
                            </span>
                          </td>
                          <td class="form-group">
                            <input id="inputHarga" type="tel" placeholder="" class="form-control">
                          </td>
                          <td class="form-group">
                            <input id="SubTotal" type="text" readonly="readonly" value="Rp. 1.000.000" class="form-control">
                          </td>
                          <td class="actions"><a href="#" class="btn btn-lg btn-space btn-primary">Simpan</a></td>
                          <!--<td class="actions"><a href="#" class="icon"><i class="mdi mdi-check"></i></a></td>-->
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
              </div>

              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">List Pembelian</div>
                <div class="card-body">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Item</th>
                          <th>Merk</th>
                          <th>Jumlah</th>
                          <th>Harga Satuan</th>
                          <th>Subtotal</th>
                          <th class="actions"></th>
                        </tr>
                      </thead>
                      <tbody class="no-border-x">
                        <tr>
                          <td>Gula</td>
                          <td>Sunlight</td>
                          <td>10 kg</td>
                          <td>10,000</td>
                          <td>Rp. 100.000</td>
                          <td class="actions">
                            <!--
                            <a href="#" class="btn btn-space btn-warning"><i class="icon mdi mdi-edit" style="color:white;"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                            -->
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>Gula</td>
                          <td>Sunlight</td>
                          <td>10 kg</td>
                          <td>10,000</td>
                          <td>Rp. 100.000</td>
                          <td class="actions">
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>Gula</td>
                          <td>Sunlight</td>
                          <td>10 kg</td>
                          <td>10,000</td>
                          <td>Rp. 100.000</td>
                          <td class="actions">
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Detail Pembelian</div>
                <div class="card-body">
                    <div class="form-group">
                      <label for="inputTanggalJamBeli">Tanggal & Jam Beli</label>
                      <input type="text" value="" class="form-control datetimepicker">
                    </div>
                    <div class="form-group pt-2">
                      <label for="inputSupplier">Nama Supplier</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="select2">
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                      </span>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputKodePembelian">Kode Pembelian</label>
                          <input id="inputKodePembelian" readonly="readonly" type="tel" value="NV-001" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputKodeNotaPembelian">Kode Nota Pembelian</label>
                          <input id="inputKodeNotaPembelian" type="tel" placeholder="" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputTotal">Grand Total</label>
                      <input for="inputTotal" type="text" readonly="readonly" value="Rp. 300.000" class="form-control">
                    </div>

                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="inputDiskonPembelian">Diskon Pembelian</label>
                          <input id="inputDiskonPembelian" type="tel" placeholder="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="inputDP">Down Payment</label>
                          <input id="inputDP" type="tel" placeholder="" class="form-control">
                        </div>
                      </div>

                      <div class="col-lg-4">
                        <div class="form-group">
                          <label for="inputKurangBayar">Kurang Bayar</label>
                          <input for="inputKurangBayar" type="text" readonly="readonly" value="Rp. 170.000" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="form-group pt-2">
                      <label for="inputSupplier">Status Penerimaan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="Belum" selected>Belum Terima</option>
                          <option value="Sudah">Sudah Terima</option>
                        </select>
                      </span>
                    </div>

                    <div class="form-group">
                      <label for="inputDP">Keterangan</label>
                      <textarea id="inputDP" class="form-control"></textarea>
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                </div>
              </div>
            </form>
            </div>


          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();

      });
    </script>
  </body>
</html>
