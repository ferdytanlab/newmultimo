<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Master Produk</div>
                <div class="card-body">
                  <form action="produk.php" data-parsley-validate="" novalidate="">
                    <div class="form-group pt-2">
                      <label for="inputNama">Jenis Produk <span style="Color:red">*</span></label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="1" selected>1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="kodeProduk">Kode Produk <span style="Color:red">*</span></label>
                      <span class="col-12 col-sm-8 col-lg-6">
                      <input id="kodeProduk" type="text" readonly="readonly" value="Readonly input text" class="form-control">
                    </span>
                    </div>
                    <div class="form-group">
                      <label for="inputNama">Nama Produk <span style="Color:red">*</span></label>
                      <input id="inputNama" type="text" name="nama" parsley-trigger="change" required="" placeholder="" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group pt-2">
                      <label for="inputNama">Satuan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="Pcs" selected>Pcs</option>
                          <option value="Box">Box</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputJual">Harga Jual <span style="Color:red">*</span></label>
                      <input id="inputJual" type="text" name="nama" parsley-trigger="change" required="" placeholder="" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputJualKaryawan">Harga Jual ke Karyawan</label>
                      <input id="inputJualKaryawan" type="tel" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputGambar">Gambar</label>
                      <span class="col-12 col-sm-6">
                        <div>
                        <input id="inputGambar" type="file" name="inputGambar" data-multiple-caption="{count} files selected" multiple class="inputfile">
                        <label for="inputGambar" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                      </div>
                    </span>
                    </div>
                    <div class="form-group">
                      <label for="inputKeterangan">Keterangan</label>
                      <textarea id="inputKeterangan" class="form-control"></textarea>
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();

      });
    </script>
  </body>
</html>
