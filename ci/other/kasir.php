<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-offcanvas-menu be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="nav-link be-toggle-left-sidebar"><span class="icon mdi mdi-menu"></span></a><a href="index.html" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Tabs-->
          <div class="row">
            <!--Default Tabs-->
            <div class="col-12 col-lg-6">
              <div class="card">
                <div class="tab-container">
                  <ul role="tablist" class="nav nav-tabs">
                    <li class="nav-item"><a href="#whole" data-toggle="tab" role="tab" class="nav-link active">Whole Bread</a></li>
                    <li class="nav-item"><a href="#bread" data-toggle="tab" role="tab" class="nav-link">Bread</a></li>
                    <li class="nav-item"><a href="#pudding" data-toggle="tab" role="tab" class="nav-link">Pudding</a></li>
                    <li class="nav-item"><a href="#cookies" data-toggle="tab" role="tab" class="nav-link">Cookies</a></li>
                    <li class="nav-item"><a href="#pastry" data-toggle="tab" role="tab" class="nav-link">Pastry</a></li>
                    <li class="nav-item"><a href="#donut" data-toggle="tab" role="tab" class="nav-link">Donut</a></li>
                    <li class="nav-item"><a href="#cake" data-toggle="tab" role="tab" class="nav-link">Cake</a></li>
                    <li class="nav-item"><a href="#tart" data-toggle="tab" role="tab" class="nav-link">Tart</a></li>
                    <li class="nav-item"><a href="#paket" data-toggle="tab" role="tab" class="nav-link">Paket</a></li>
                  </ul>
                  <div class="tab-content" style="min-height:450px; max-height: 450px; overflow:scroll;">
                    <div id="whole" role="tabpanel" class="tab-pane active">
                      <div class="row">
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-a.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread A</button></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-b.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread B</button></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-c.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread C</button></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-d.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread D</button></span>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div id="bread" role="tabpanel" class="tab-pane">
                      <div class="row">
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-a.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread A</button></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-d.jpg" width="100px;" height="auto"></span>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Bread D</button></span>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div id="pudding" role="tabpanel" class="tab-pane">
                      <div class="row">
                        <div class="col-12 col-lg-3 col-sm-4 col-xs-6">
                          <img src="assets/img/orders.png">
                          <p>Bread A</p>
                        </div>
                      </div>
                    </div>
                    <div id="paket" role="tabpanel" class="tab-pane">
                      <div class="row">
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-a.jpg" width="100px;" height="auto"></span>
                                <span><a href="#" data-toggle="modal" data-target="#custom">
                                  <!--<p style="font-size:16px;">Paket Custom</p>-->
                                  <button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Paket Custom</button>
                                </a></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-xs-6">
                            <div class="bs-grid-block">
                              <div class="content">
                                <span class="size"><img src="assets/img/bread-b.jpg" width="100px;" height="auto"></span>
                                <p style="font-size:16px;">Paket Bukan Custom</p>
                                <span><button class="btn btn-rounded btn-space btn-secondary mt-4 mb-2">Paket Bukan Custom</button></span>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card">
                <div class="card-body" style="min-height:500px;">
                  <form action="pemesanan.php" data-parsley-validate="" novalidate="">
                    <div class="form-group">
                      <select class="select2 form-control">
                        <option>Walk-in Customer</option>
                        <option>Ferdy (ferdy@ferdytan.com / 08563098912)</option>
                        <option>Paijo (paijo@gmail.com / 08123451232)</option>
                        <option>Sumiati (sumiati@gmail.com / 08381230543)</option>
                        <option>Moana (moana@gmail.com / 082234991234)</option>
                        <option>Doddy (doddy@gmail.com / 08112343065)</option>
                        <option>Munarsih (munarsih@ferdytan.com / 081823023453)</option>
                        <option>Vicky (vicky@ferdytan.com / 082109864830)</option>
                      </select>
                      <span class="input-group-btn">
                        <a href="masterMembers.php"><button class="btn btn-primary" type="button">Add New</button></a>
                      </span>
                    </div>
                    <div class="be-scroller">
                      <div class="table-responsive" style="height:284px">
                        <table class="table table-striped table-vcenter">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th width="10px">Products</th>
                              <th width="100px"></th>
                              <th>Qty</th>
                              <th>Cost</th>
                              <th>Total</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td><img src="assets/img/bread-a.jpg" width="50" height="auto"></td>
                              <td align="left" width="50px;">Bread A</td>
                              <td><span ><input type="tel" size="3"> </span></td>
                              <td>Rp. 27,000</td>
                              <td>Rp. 27,000</td>
                              <td><a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a></td>
                            </tr>

                            <tr>
                              <td>2</td>
                              <td><img src="assets/img/bread-b.jpg" width="50" height="auto"></td>
                              <td align="left" width="50px;">Bread B</td>
                              <td><span ><input type="tel" size="3"> </span></td>
                              <td>Rp. 21,000</td>
                              <td>Rp. 21,000</td>
                              <td><a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a></td>
                            </tr>

                            <tr>
                              <td>3</td>
                              <td><img src="assets/img/bread-c.jpg" width="50" height="auto"></td>
                              <td align="left" width="50px;">Bread C</td>
                              <td><span ><input type="tel" size="3"> </span></td>
                              <td>Rp. 24,000</td>
                              <td>Rp. 24,000</td>
                              <td><a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a></td>
                            </tr>

                            <tr>
                              <td>3</td>
                              <td><img src="assets/img/bread-c.jpg" width="50" height="auto"></td>
                              <td align="left" width="50px;">Bread C</td>
                              <td><span ><input type="tel" size="3"> </span></td>
                              <td>Rp. 24,000</td>
                              <td>Rp. 24,000</td>
                              <td><a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div>
                      <table class="table">
                        <tbody>
                          <!--
                          <tr>
                            <td width="60%">Subtotal</td>
                            <td align="right" style="font-size: large; font-weight: bold;">Rp. 74,000</td>
                          </tr>

                          <tr>
                            <td width="60%">Discount</td>
                            <td align="right" style="font-size: large; font-weight: bold;"id="demo-editable-firstname" data-type="text" data-pk="1" data-placement="left" data-placeholder="" data-title="Input Discount"</a></td>
                          </tr>
                        -->
                          <tr>
                            <td width="60%">Subtotal</td>
                            <td align="right" style="font-size: 30px; font-weight: bold;">Rp. 74,000</td>
                          </tr>
                        </tbody>
                      </table>

                    <div>
                      <div class="btn-toolbar">
                        <div role="group" class="btn-group btn-group-justified btn-space">
                          <a href="#" class="btn btn-lg btn-secondary">Batal</a>
                          <a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target="#bayar">Lanjut</a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
      	App.formElements();
      });

      $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
      });
    </script>
    <script type="text/javascript">
      $(window).on('load',function(){
          $('#md-footer-primary').modal('show');
      });
    </script>

    <div id="bayar" tabindex="-1" role="dialog" class="modal fade">
  		<div class="modal-dialog">
  		  <div class="modal-content">
  				<!--Modal header-->
  				  <div class="modal-header">
  					</div>

          <!--Modal body-->
  				  <div class="modal-body text-center">
  					  <table width="80%" align="center">
  						  <tr>
  							  <td><p style="font-size: 30px; font-weight: bold;" align="left">Pembayaran</p></td>
  							</tr>
  						  <tr>
  							  <td><p class="text semi-bold text-main mb-2" align="left">Transaksi</p>

                  </td>
  							  <td align="right"><p style="font-size: 28px;">Rp 74,000</p>
                    <span class="card-subtitle">Total Barang : 4 item</span>
    							</td>
  						  </tr>

  							<tr>
  							  <td><p class="text semi-bold text-main" align="left">Diskon</p></td>
                  <td align="right" width="50%">
                    <div class="input-group mb-1">
                      <input type="text" placeholder="Rp." class="form-control text-right">
                    </div>
                  </td>
  						  </tr>
                <tr>
  							  <td><p class="text semi-bold text-main" align="left">Kode Voucher</p></td>
                  <td align="right" width="50%">
                    <div class="input-group">
                      <input type="text" class="form-control">
                      <div class="input-group-append">
                        <button type="button" class="btn btn-primary">Cek</button>
                      </div>
                    </div>
                    <span class="card-subtitle mb-2">Potongan : Rp. 10.000</span>
                  </td>
  						  </tr>
                <tr>
  							  <td>&nbsp;</td>
  						  </tr>
                <tr class="mt-3">
  							  <td><p class="text semi-bold text-main" align="left">Grand Total</p></td>
  							  <td align="right"><p style="font-size: 30px; font-weight: bold;">Rp 64,000</p></td>
  						  </tr>
  						  <tr>
                  <td><p class="text semi-bold text-main" align="left">Metode Pembayaran</p></td>
                  <td align="right">
                    <div class="radio">
                      <!-- Inline radio buttons -->
                      <input id="demo-inline-form-radio" class="magic-radio" type="radio" name="inline-form-radio" checked>
                      <label for="demo-inline-form-radio">Tunai</label>
                      <input id="demo-inline-form-radio-2" class="magic-radio" type="radio" name="inline-form-radio">
                      <label for="demo-inline-form-radio-2">Kartu Debit / Kredit</label>
                    </div>
  							  </td>
  						  </tr>
  						  <tr>
                  <td><p class="text semi-bold text-main" align="left">Tunai</p></td>
                  <td align="right" width="50%">
                    <div class="input-group mar-btm">
                      <input type="text" placeholder="Rp." class="form-control text-right">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><p class="text semi-bold text-main" align="left">Kembalian</p></td>
                  <td align="right"><p style="font-size: 30px; font-weight: bold;">Rp 1.000</p></td>
                </tr>
                <tr class="mt-2">
                  <td>&nbsp;</td>
                  <td align="right">
                    <br>
                    <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary btn-space modal-close">Batal</button>
                    <button type="button" data-dismiss="modal" class="btn btn-lg btn-success btn-space modal-close">Bayar</button>
                  </td>
                </tr>
              </table>
            </div>

            <div class="modal-footer"></div>
        </div>
      </div>
    </div>

    <div id="custom" tabindex="-1" role="dialog" class="modal fade">
  		<div class="modal-dialog">
  		  <div class="modal-content">
  				<!--Modal header-->
  				  <div class="modal-header">
  					</div>

          <!--Modal body-->
  				  <div class="modal-body text-center">
              <table class="table table-striped table-vcenter">
                <thead>
                  <tr>
                    <th>#</th>
                    <th width="10px">Products</th>
                    <th width="100px"></th>
                    <th>Qty</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td><img src="assets/img/bread-a.jpg" width="50" height="auto"></td>
                    <td align="left" width="50px;">Bread A</td>
                    <td><span ><input type="tel"> </span></td>
                  </tr>

                  <tr>
                    <td>2</td>
                    <td><img src="assets/img/bread-b.jpg" width="50" height="auto"></td>
                    <td align="left" width="50px;">Bread B</td>
                    <td><span ><input type="tel"> </span></td>
                  </tr>

                  <tr>
                    <td>3</td>
                    <td><img src="assets/img/bread-c.jpg" width="50" height="auto"></td>
                    <td align="left" width="50px;">Bread C</td>
                    <td><span ><input type="tel"> </span></td>
                  </tr>

                  <tr>
                    <td>3</td>
                    <td><img src="assets/img/bread-c.jpg" width="50" height="auto"></td>
                    <td align="left" width="50px;">Bread C</td>
                    <td><span ><input type="tel"> </span></td>
                  </tr>
              </table>
              <div class="mt-8">
                <button type="button" data-dismiss="modal" class="btn btn-lg btn-secondary">Batal</button>
                <button type="button" data-dismiss="modal" class="btn btn-lg btn-primary">Ubah</button>
              </div>
            </div>

            <div class="modal-footer"></div>
        </div>
      </div>
    </div>
  </body>
</html>
