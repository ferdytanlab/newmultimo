<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Master Item</div>
                <div class="card-body">
                  <form action="item.php">
                    <div class="form-group pt-2">
                      <label for="inputKode">Kode Barang</label>
                      <input id="inputKode" type="test" placeholder="" class="form-control">
                    </div>
                    <div class="form-group pt-2">
                      <label for="inputJenis">Jenis Barang</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="1" selected>1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputNamaBarang">Nama Barang</label>
                      <input id="inputNamaBarang" type="text" placeholder="" class="form-control">
                    </div>
                    <div class="form-group pt-2">
                      <label for="inputSatuan">Satuan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="Pcs" selected>Pcs</option>
                          <option value="Box">Box</option>
                        </select>
                      </span>
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Tambah Merk</div>
                <div class="card-body">
                  <form data-parsley-validate="" novalidate="">
                    <table class="table">
                      <thead>
                        <tr>
                          <th style="width:50%;">Nama Merk</th>
                          <th class="actions"></th>
                        </tr>
                      </thead>
                      <tbody class="no-border-x">
                        <tr>
                          <td>
                            <span class="col-12 col-sm-8 col-lg-6">
                              <select class="form-control">
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                            </span>
                          </td>
                          <td class="actions"><a href="#" class="btn btn-lg btn-space btn-primary">Simpan</a></td>
                          <!--<td class="actions"><a href="#" class="icon"><i class="mdi mdi-check"></i></a></td>-->
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
              </div>

              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">List Merk</div>
                <div class="card-body">
                    <table class="table">
                      <thead>
                        <tr>
                          <th style="width:50%;">Nama Merk</th>
                          <th class="actions"></th>
                        </tr>
                      </thead>
                      <tbody class="no-border-x">
                        <tr>
                          <td>Merah</td>
                          <td class="actions">
                            <!--
                            <a href="#" class="btn btn-space btn-warning"><i class="icon mdi mdi-edit" style="color:white;"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                            -->
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>Kuning</td>
                          <td class="actions">
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>Biru</td>
                          <td class="actions">
                            <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                            <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
        App.formElements();

      });
    </script>
  </body>
</html>
