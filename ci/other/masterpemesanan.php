<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Master Pemesanan</div>
                <div class="card-body">
                  <form action="pemesanan.php" data-parsley-validate="" novalidate="">
                    <div class="form-group">
                      <label for="inputNo">Nomor Pemesanan</label>
                      <input id="inputNo" type="text" readonly="readonly" value="V/001/04/2018" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputTanggalJam">Tanggal & Jam</label>
                      <input type="text" value="" class="form-control datetimepicker">
                    </div>
                    <div class="form-group pt-2">
                      <label for="inputSupplier">Sumber Pesanan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="select2">
                            <option value="1">Whatsapp</option>
                            <option value="2">Reseller</option>
                            <option value="2">Toko</option>
                            <option value="2">Other</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputNama">Nama Pemesan</label>
                      <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputKeterangan">Keterangan Pesanan</label>
                      <textarea id="inputKeterangan" class="form-control"></textarea>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputTCoklat">Text Coklat</label>
                          <input id="inputTCoklat" type="text" placeholder="" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputTKartu">Text Kartu Ucapan</label>
                          <input id="inputTKartu" type="text" placeholder="" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputNamaPenerima">Nama Penerima</label>
                      <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputTelPenerima">No Telp Penerima</label>
                      <input type="tel" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputAlamatPenerima">Alamat Penerima</label>
                      <input type="address" placeholder="" class="form-control">
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputTanggalAmbil">Tanggal Diambil</label>
                          <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                            <input size="16" type="text" value="" class="form-control">
                            <div class="input-group-append">
                              <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputJamSelesai">Pesanan harus selesai jam</label>
                          <div class="mt-1">
                          <span style="display:inline-block">
                            <select class="form-control">
                                <option value="00">00</option>
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                            </select>
                          </span>
                          :
                          <span style="display:inline-block">
                            <select class="form-control">
                              <option value="00">00</option>
                              <option value="01">01</option>
                              <option value="02">02</option>
                              <option value="03">03</option>
                              <option value="04">04</option>
                              <option value="05">05</option>
                              <option value="06">06</option>
                              <option value="07">07</option>
                              <option value="08">08</option>
                              <option value="09">09</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                              <option value="21">21</option>
                              <option value="22">22</option>
                              <option value="23">23</option>
                              <option value="24">24</option>
                              <option value="25">25</option>
                              <option value="26">26</option>
                              <option value="27">27</option>
                              <option value="28">28</option>
                              <option value="29">29</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                              <option value="32">32</option>
                              <option value="33">33</option>
                              <option value="34">34</option>
                              <option value="35">35</option>
                              <option value="36">36</option>
                              <option value="37">37</option>
                              <option value="38">38</option>
                              <option value="39">39</option>
                              <option value="40">40</option>
                              <option value="41">41</option>
                              <option value="42">42</option>
                              <option value="43">43</option>
                              <option value="44">44</option>
                              <option value="45">45</option>
                              <option value="46">46</option>
                              <option value="47">47</option>
                              <option value="48">48</option>
                              <option value="49">49</option>
                              <option value="50">50</option>
                              <option value="51">51</option>
                              <option value="52">52</option>
                              <option value="53">53</option>
                              <option value="54">54</option>
                              <option value="55">55</option>
                              <option value="56">56</option>
                              <option value="57">57</option>
                              <option value="58">58</option>
                              <option value="59">59</option>
                            </select>
                          </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputAlamatPenerima">Status</label>
                      <span>
                        <select class="form-control">
                            <option value="Ambil">Ambil Sendiri</option>
                            <option value="Kirim">Kirim</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputNamaProduk">Nama Produk</label>
                      <span>
                        <select class="select2">
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                      </span>
                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inputHargaProduk">Harga Produk</label>
                          <input type="tel" placeholder="" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="inpuOngkir">Ongkir</label>
                          <input type="tel" placeholder="" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputDP">Down Payment</label>
                      <input type="tel" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputSisaBayar">Sisa Bayar</label>
                      <input type="tel" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputKeterangan">Keterangan Pesanan</label>
                      <textarea id="inputKeterangan" class="form-control"></textarea>
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Status Pemesanan</div>
                <div class="card-body">
                  <form action="pemesanan.php" data-parsley-validate="" novalidate="">
                    <div class="form-group">
                      <label for="inputNamaPemesan">Tampilkan Nama Pemesan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Tampilkan</option>
                            <option value="2">Sembunyikan</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputStatusPesanan">Status Pesanan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Waiting List</option>
                            <option value="2">On Proses</option>
                            <option value="3">Proses</option>
                            <option value="4">Finish</option>
                            <option value="5">On Delivery</option>
                            <option value="6">Delivered</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputStatusPelunasan">Status Pelunasan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Belum Lunas</option>
                            <option value="2">Lunas</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputPembayaranDi">Pembayaran Di</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Toko</option>
                            <option value="2">COD</option>
                            <option value="2">Transfer</option>
                        </select>
                      </span>
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
      	$('form').parsley();
      	App.formElements();

      });
    </script>
  </body>
</html>
