<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>FPOS</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Master Pelunasan</div>
                <div class="card-body">
                  <form action="pelunasan.php" data-parsley-validate="" novalidate="">
                    <div class="form-group">
                      <label for="inputNo">Nomor Pemesanan</label>
                      <span>
                        <select class="select2">
                            <option value="1">V/001/04/2018</option>
                            <option value="2">V/002/04/2018</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputTanggal">Tanggal Pesanan</label>
                      <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                        <input size="16" type="text" value="" class="form-control">
                        <div class="input-group-append">
                          <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputNamaPemesan">Nama Pemesan</label>
                      <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputTanggalPesanan">Tanggal Pesanan</label>
                      <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                        <input size="16" type="text" value="" class="form-control">
                        <div class="input-group-append">
                          <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputTotal">Grand Total</label>
                      <input id="inputTotal" type="text" readonly="readonly" value="Rp. 240.500" class="form-control">
                    </div>

                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Status Pelunasan</div>
                <div class="card-body">
                  <form action="pelunasan.php" data-parsley-validate="" novalidate="">
                    <div class="form-group">
                      <label for="inputStatusPelunasan">Status Pelunasan</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Belum Lunas</option>
                            <option value="2">Lunas</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputTanggalLunas">Tanggal Lunas</label>
                      <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                        <input size="16" type="text" value="" class="form-control">
                        <div class="input-group-append">
                          <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputStatusPelunasan">Jenis Pembayaran</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">Tunai</option>
                            <option value="2">Debit</option>
                            <option value="3">Transfer</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputBank">Bank</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                            <option value="1">BCA</option>
                            <option value="2">Mandiri</option>
                            <option value="3">Permata</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputBank">Bank</label>
                      <input type="text" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputNominal">Nominal Transfer</label>
                      <input type="tel" placeholder="" class="form-control">
                    </div>
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-primary">Simpan</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
      	$('form').parsley();
      	App.formElements();

      });
    </script>
  </body>
</html>
