<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Dashboard ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="index.php" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
                        <div class="widget widget-tile">
                          <img src="assets/img/orders.png">
                          <div class="data-info">
                            <div class="desc">Produk</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="113" class="number">0</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                        <div class="widget widget-tile">
                          <img src="assets/img/sales-money.png">
                          <div class="data-info">
                            <div class="desc">Penjualan</div>
                            <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter" data-end="80" data-suffix="%" class="number">0</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                        <div class="widget widget-tile">
                          <img src="assets/img/orders.png">
                          <div class="data-info">
                            <div class="desc">Jumlah Order</div>
                            <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span data-toggle="counter" data-end="532" class="number">0</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
                        <div class="widget widget-tile">
                          <img src="assets/img/sales-money.png">
                          <div class="data-info">
                            <div class="desc">Pending Order</div>
                            <div class="value"><span class="indicator indicator-negative mdi mdi-chevron-down"></span><span data-toggle="counter" data-end="113" class="number">0</span>
                            </div>
                          </div>
                        </div>
            </div>
          </div>
          <div class="row">
          <div class="col-12 col-lg-8">
              <div class="card card-table">
                <div class="card-header">
                  <div class="title">Purchases</div>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-striped table-borderless">
                    <thead>
                      <tr>
                        <th style="width:30%;">Product</th>
                        <th >Price</th>
                        <th style="width:20%;">Date</th>
                        <th style="width:20%;">State</th>
                        <th style="width:5%;" class="actions"></th>
                      </tr>
                    </thead>
                    <tbody class="no-border-x">
                      <tr>
                        <td>Fruit Cake</td>
                        <td>Rp.50.000</td>
                        <td>1 Jan 2018</td>
                        <td class="text-success">Completed</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                      <tr>
                        <td>Apple Pie</td>
                        <td>Rp.45.000</td>
                        <td>1 Feb 2018</td>
                        <td class="text-success">Completed</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                      <tr>
                        <td>Donut</td>
                        <td>Rp.18.000</td>
                        <td>18 Feb 2018</td>
                        <td class="text-warning">Pending</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                      <tr>
                        <td>Brownies</td>
                        <td>Rp.68.000</td>
                        <td>28 Feb 2018</td>
                        <td class="text-warning">Pending</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                      <tr>
                        <td>Cheese Cake</td>
                        <td>Rp.55.000</td>
                        <td>4 Mar 2018</td>
                        <td class="text-danger">Cancelled</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                      <tr>
                        <td>Mix Macarons</td>
                        <td>Rp.47.500</td>
                        <td>4 Mar 2018</td>
                        <td class="text-success">Completed</td>
                        <td class="actions"><a href="#" class="icon"><i class="mdi mdi-caret-right-circle"></i></a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <div class="col-12 col-lg-4">
              <div class="widget widget-calendar">
                <div id="calendar-widget"></div>
              </div>
            </div>
          </div>

          <div class="row" style="visibility: hidden; margin-top: -170px; z-index: -1;">
            <div class="col-md-12">
              <div class="widget widget-fullwidth be-loading">
                <div class="widget-head">
                  <div class="tools">
                    <div class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert d-inline-block d-md-none"></span></a>
                      <div role="menu" class="dropdown-menu"><a href="index.html#" class="dropdown-item">Week</a><a href="index.html#" class="dropdown-item">Month</a><a href="index.html#" class="dropdown-item">Year</a>
                        <div class="dropdown-divider"></div><a href="index.html#" class="dropdown-item">Today</a>
                      </div>
                    </div><span class="icon mdi mdi-chevron-down"></span><span class="icon toggle-loading mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span>
                  </div>
                  <div class="button-toolbar d-none d-md-block">
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary">Week</button>
                      <button type="button" class="btn btn-secondary active">Month</button>
                      <button type="button" class="btn btn-secondary">Year</button>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary">Today</button>
                    </div>
                  </div><span class="title">Recent Movement</span>
                </div>
                <div class="widget-chart-container">
                  <div class="widget-chart-info">
                    <ul class="chart-legend-horizontal">
                      <li><span data-color="main-chart-color1"></span> Purchases</li>
                      <li><span data-color="main-chart-color2"></span> Plans</li>
                      <li><span data-color="main-chart-color3"></span> Services</li>
                    </ul>
                  </div>
                  <div class="widget-counter-group widget-counter-group-right">
                    <div class="counter counter-big">
                      <div class="value">25%</div>
                      <div class="desc">Purchase</div>
                    </div>
                    <div class="counter counter-big">
                      <div class="value">5%</div>
                      <div class="desc">Plans</div>
                    </div>
                    <div class="counter counter-big">
                      <div class="value">5%</div>
                      <div class="desc">Services</div>
                    </div>
                  </div>
                  <div id="main-chart" style="height: 1px;"></div>
                </div>
                <div class="be-spinner">
                  <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                  </svg>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      	App.dashboard();

      });
    </script>
  </body>
</html>
