<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Modenta UV Printing ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="index.html" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title">Modenta UV Printing Item
	           <span>
	           <a href="mastermItems.php"><button class="btn btn-space btn-primary mx-5"><i class="icon icon-left mdi mdi-plus"></i> Add New</button>
	           </a>
		       </span>
          </h2>
        </div>
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card card-table">
                <!--<div class="card-header">List Jenis Item</div>-->
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th class="actions"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td><img src="../assets/img/modenta/m-1.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td><img src="../assets/img/modenta/m-2.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td><img src="../assets/img/modenta/m-3.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td><img src="../assets/img/modenta/m-4.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td><img src="../assets/img/modenta/m-5.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td><img src="../assets/img/modenta/m-6.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>7</td>
                        <td><img src="../assets/img/modenta/m-7.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>8</td>
                        <td><img src="../assets/img/modenta/m-8.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>9</td>
                        <td><img src="../assets/img/modenta/m-9.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>10</td>
                        <td><img src="../assets/img/modenta/m-10.jpg" width="auto" height="25%"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card card-table">
                <!--<div class="card-header">List Jenis Item</div>-->
                <div class="card-body">


	              <a href="mastermItems.php">
	                <h3 class="page-head-title text-center">
		                <br>
		                <img src="assets/img/addsome.jpg"><br>
		                Add modenta items image details to get started</h3>
	              </a>
	              <br><br>
              </div>
            </div>
            </div>
          </div>



         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
      	$('form').parsley();
      	App.formElements();

      });
    </script>
  </body>
</html>
