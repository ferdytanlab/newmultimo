<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Product Gallery ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="index.html" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title">Product Gallery
	           <span>
	           <a href="masterProductGallery.php"><button class="btn btn-space btn-primary mx-5"><i class="icon icon-left mdi mdi-plus"></i> Add New</button>
	           </a>
		       </span>
          </h2>
        </div>
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-12">
              <div class="card card-table">
                <!--<div class="card-header">List Jenis Item</div>-->

                <div class="card-header">List Product Gallery
                  <div class="tools dropdown">

                  </div>
                </div>
                <div class="card-body">
                  <table id="table1" class="table table-striped table-hover table-fw-widget">
                    <thead>
                      <tr>

                        <th>Image</th>
                        <th>Product Gallery</th>

                        <th class="actions"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>

                        <td><img src="../assets/img/project/gsign hotel bali 1.jpg" width="auto" height="40px"></td>
                        <td>G'SIGN SYTLE HOTEL BALI</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                      <tr>

                        <td><img src="../assets/img/project/1BRIGHTON.jpg" width="auto" height="40px"></td>
                        <td>Brighton Surabaya</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                      <tr>

                        <td><img src="../assets/img/project/1Viladelvia-makasar.jpg" width="auto" height="40px"></td>
                        <td>GBI Filadelfia GCI mall Makasar</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>

                      <tr>

                        <td><img src="../assets/img/project/gbi-ka.jpg" width="auto" height="40px"></td>
                        <td>GBI Keluarga Allah Surabaya</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                      <tr>

                        <td><img src="../assets/img/project/midltown 2.jpg" width="auto" height="40px"></td>
                        <td>Midtown Residence Surabaya</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                      <tr>

                        <td><img src="../assets/img/project/ayola website 1.jpg" width="auto" height="40px"></td>
                        <td>Ayola La Lisa</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>

                      <tr>

                        <td><img src="../assets/img/project/AlUswa.jpg" width="auto" height="40px"></td>
                        <td>SMA-IT Al Uswah Surabaya</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>

                      <tr>

                        <td><img src="../assets/img/project/GMS 1.jpg" width="auto" height="40px"></td>
                        <td>Gereja Mawar Sharon</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                      <tr>

                        <td><img src="../assets/img/project/sea monster 1.jpg" width="auto" height="40px"></td>
                        <td>Sea Monster Restaurant Surabaya</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>

                      <tr>

                        <td><img src="../assets/img/project/Shalimar A 1.jpg" width="auto" height="40px"></td>
                        <td>The Shalimar Boutique Hotel</td>
                        <td>2017</td>
                        <td class="actions text-right">
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-facebook"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-secondary btn-social btn-danger"><i class="icon mdi mdi-delete" style="color:white"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!--
          <div class="row">
            <div class="col-sm-12">
              <div class="card card-table">
                <div class="card-body">


	              <a href="masterProject.php">
	                <h3 class="page-head-title text-center">
		                <br>
		                <img src="assets/img/addsome.jpg"><br>
		                Add project to get started</h3>
	              </a>
	              <br><br>
              </div>
            </div>
            </div>
          </div>
        -->


         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
      	$('form').parsley();
      	App.formElements();
        App.dataTables();

      });
    </script>
  </body>
</html>
