<?php ?>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
              <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><img src="assets/img/avatar.gif" alt="Avatar"><span class="user-name">Administrator</span></a>
                <div role="menu" class="dropdown-menu">
                  <div class="user-info">
                    <div class="user-name">Administrator</div>
                    <div class="user-position online">Available</div>
                  </div><a href="pages-profile.html" class="dropdown-item"><span class="icon mdi mdi-face"></span> Account</a><a href="index.html#" class="dropdown-item"><span class="icon mdi mdi-settings"></span> Settings</a><a href="pages-login.html" class="dropdown-item"><span class="icon mdi mdi-power"></span> Logout</a>
                </div>
              </li>
            </ul>

            <div class="page-title"><span>Multimo Back Office</span></div>

          </div>
        </div>
      </nav>
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li class="divider">Menu</li>
                  <li class=""><a href="dashboard.php"><i class="icon mdi mdi-view-dashboard"></i><span>Dashboard</span></a>
                  </li>
                  <li class=" parent"><a href="#"><i class="icon mdi mdi-image-alt"></i><span>Homepage Header</span></a>
                    <ul class="sub-menu">
                      <li><a href="cHeader.php">Carousel Header</a>
                      </li>
                      <li><a href="dHeader.php">Divider</a>
                      </li>
                      <li><a href="mHeader.php">Modenta Header</a>
                      </li>
                    </ul>
                  </li>

                  <li class=" parent"><a href="#"><i class="icon mdi mdi-seat"></i><span>Product</span></a>
                    <ul class="sub-menu">
                      <li><a href="category.php">Category</a>
                      </li>
                      <li><a href="brand.php">Brand</a>
                      </li>
                      <li><a href="product.php">Product</a>
                      </li>
                      <li class="parent"><a href="#"><span>Material</span></a>
                        <ul class="sub-menu">
                          <li><a href="pipe.php">Pipe</a>
                          </li>
                          <li><a href="cover.php">Cover</a>
                          </li>
                          <li><a href="tabletop.php">Table Top</a>
                          </li>
                          <!--
                          <li><a href="glass.php">Glass</a>
                          </li>
                          -->
                        </ul>
                      </li>
                      <li><a href="marketplace.php">Marketplace</a>
                      </li>
                    </ul>
                  </li>

                  <li class=""><a href="productGallery.php"><i class="icon mdi mdi-collection-bookmark"></i><span>Product Gallery</span></a>
                  </li>

                  <li class=""><a href="news.php"><i class="icon mdi mdi-collection-text"></i><span>News & Tips</span></a>
                  </li>

                  <li class=""><a href="testimonial.php"><i class="icon mdi mdi-format-list-bulleted"></i><span>Testimonial</span></a>
                  </li>

                  <li class=""><a href="modentaUV.php"><i class="icon mdi mdi-print"></i><span>Modenta UV Printing</span></a>
                  </li>

                  <li class=""><a href="job.php"><i class="icon mdi mdi-account-add"></i><span>Job Vacancy</span></a>
                  </li>

                  <li class=""><a href="media.php"><i class="icon mdi mdi-archive"></i><span>Media</span></a>
                  </li>

                  <li class=""><a href="agent.php"><i class="icon mdi mdi-account"></i><span>Agent</span></a>
                  </li>


                  <li class=" parent"><a href="#"><i class="icon mdi mdi-store"></i><span>Stockist</span></a>
                    <ul class="sub-menu">
                      <li><a href="region.php">Region</a>
                      </li>
                      <li><a href="city.php">City</a>
                      </li>
                      <li><a href="store.php">Store</a>
                      </li>
                      <li><a href="storeCategory.php">Store Category</a>
                      </li>
                    </ul>
                  </li>

                  <li class=""><a href="countclick.php"><i class="icon mdi mdi-trending-up"></i><span>Count Click</span></a>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
