<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Edit Product ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/summernote/summernote-bs4.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Product Testimony</div>
                <div class="card-body">
                  <form action="product.php">
                    <div class="form-group">
                      <label for="inputNo">Product</label>
                      <input id="inputNo" type="text" readonly="readonly" value="Seahorse HEART Stainless" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputName">Name </label>
                      <input type="text" name="name" placeholder=""  class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputTestimony">Testimony</label>
                      <textarea id="inputTestimony" class="form-control"></textarea>
                    </div>


                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-lg btn-primary toggle-loading">Save</button>

                          <div class="be-spinner">
                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                              <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                            </svg>
                          </div>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">List Testimony</div>
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                    
                        <th>Name</th>
                        <th>Testimony</th>
                        <th class="actions" width="25%"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <!--
                      <tr>
                        <td>1</td>
                        <td>Yudi</td>
                        <td>Pengiriman tepat waktu, barang sesuai harapan</td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Ferdy</td>
                        <td>After Sales Service bagus. saya puas memakai produk ini</td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                    -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();
        App.loaders();
        App.textEditors();
      	//Runs prettify
      	prettyPrint();

      });
    </script>
  </body>
</html>
