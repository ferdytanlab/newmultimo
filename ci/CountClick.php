<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Count Click ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/summernote/summernote-bs4.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-12">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Periode</div>
                <div class="card-body">
                  <form action="countclick.php">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-3">
                          <label for="inputYear">From </label>
                          <div>
                            <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                              <input size="16" type="text" value="" class="form-control">
                              <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-sm-3">
                          <label for="inputYear">To </label>
                          <div>
                            <div data-min-view="2" data-date-format="dd-mm-yyyy" class="input-group date datetimepicker">
                              <input size="16" type="text" value="" class="form-control">
                              <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-sm-3">
                          <label for="inputYear" style="visibility:hidden;" class="pt-1">To </label>
                          <p>
                            <button class="btn btn-primary btn-xl toggle-loading">Search</button>

                            <div class="be-spinner">
                              <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                              </svg>
                            </div>
                          </p>
                        </div>
                      </div>
                    </div>



                  </form>
                </div>

              </div>

              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Result</div>
                  <div class="card-body">
                    <div class="row">
                      <!-- Start Copy Here -->
                      <div class="col-lg-12">
                        <div class="card card-table">
                          <div class="card-header">Bali
                            <div class="tools dropdown">
                            </div>
                          </div>
                          <div class="card-body">
                            <table class="table table-striped table-borderless">
                              <thead>
                                <tr>
                                  <th style="width:20%;">No</th>
                                  <th style="width:50%;">City</th>
                                  <th>Click</th>
                                </tr>
                              </thead>
                              <tbody class="no-border-x">
                                <tr>
                                  <td>1</td>
                                  <td>Denpasar</td>
                                  <td>15</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Nusa Penida</td>
                                  <td>21</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <!-- End Here -->

                      <!-- Start Copy Here -->
                      <div class="col-lg-12">
                        <div class="card card-table">
                          <div class="card-header">Jawa Timur
                            <div class="tools dropdown">
                            </div>
                          </div>
                          <div class="card-body">
                            <table class="table table-striped table-borderless">
                              <thead>
                                <tr>
                                  <th style="width:20%;">No</th>
                                  <th style="width:50%;">City</th>
                                  <th>Click</th>
                                </tr>
                              </thead>
                              <tbody class="no-border-x">
                                <tr>
                                  <td>1</td>
                                  <td>Surabaya</td>
                                  <td>15</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Malang</td>
                                  <td>21</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <!-- End Here -->
                    </div>
                  </div>
                </div>
              </div>
            </div>


          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();
        App.loaders();
        App.textEditors();
      	//Runs prettify
      	prettyPrint();

      });
    </script>
  </body>
</html>
