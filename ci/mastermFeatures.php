<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Edit Page ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-12">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Features 1<span class="card-subtitle">Image Size : 1200 x 430px</span></div>
                <div class="card-body">
                  <form action="mFeatures.php" data-parsley-validate="" novalidate="">
                    <div class="form-group row">
                      <label for="file-1" class="col-12 col-sm-3 col-form-label">Upload Image</label>
                      <div class="col-12 col-sm-6">
                        <input id="inputGambar" type="file" name="inputGambar" data-multiple-caption="{count} files selected" multiple class="inputfile">
                        <label for="inputGambar" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <input type="text" name="name" placeholder="Title"  class="form-control">
                    </div>

                    <div class="form-group">
                    <label for="inputContent">Content</label>
                      <textarea id="inputContent" class="form-control"></textarea>
                    </div>

                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-lg btn-primary toggle-loading">Save</button>

                          <div class="be-spinner">
                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                              <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                            </svg>
                          </div>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Features 2<span class="card-subtitle">Image Size : 1200 x 430px</span></div>
                <div class="card-body">
                  <form action="mFeatures.php" data-parsley-validate="" novalidate="">
                    <div class="form-group row">
                      <label for="file-1" class="col-12 col-sm-3 col-form-label">Upload Image</label>
                      <div class="col-12 col-sm-6">
                        <input id="inputGambar" type="file" name="inputGambar" data-multiple-caption="{count} files selected" multiple class="inputfile">
                        <label for="inputGambar" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                      </div>
                    </div>

                    <div class="form-group">
                      <input type="text" name="name" placeholder="Title"  class="form-control">
                    </div>

                    <div class="form-group">
                    <label for="inputContent">Content</label>
                      <textarea id="inputContent" class="form-control"></textarea>
                    </div>

                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-lg btn-primary toggle-loading">Save</button>

                          <div class="be-spinner">
                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                              <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                            </svg>
                          </div>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();
        App.loaders();

      	//Runs prettify
      	prettyPrint();

      });
    </script>
  </body>
</html>
