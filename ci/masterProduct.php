<?php

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Edit Product ‹ Multimo</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/summernote/summernote-bs4.css"/>
    <link rel="stylesheet" href="assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a href="#" class="navbar-brand"></a>
          </div>
          <?php include ('nav.php') ?>
      <div class="be-content">
        <div class="main-content container-fluid">
          <!--Basic forms-->
          <div class="row">
            <div class="col-lg-6">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Master Product</div>
                <div class="card-body">
                  <form action="product.php">
                    <div class="form-group pt-2">
                      <label for="inputNama">Select Brand</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select class="form-control">
                          <option value="1" selected>Seahorse</option>
                          <option value="2">Classmate</option>
                          <option value="3">Goodliving</option>
                          <option value="4">Modenta</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputNama">Select Category</label>
                      <span class="col-12 col-sm-8 col-lg-6">
                        <select multiple="" class="select2">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </span>
                    </div>
                    <div class="form-group">
                      <label for="inputName">Product Name </label>
                      <input type="text" name="name" placeholder=""  class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputCode">Product Code / SKU </label>
                      <input type="text" name="code" placeholder=""  class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="inputDesc">Product Description</label>
                      <textarea id="inputDesc" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="inputDesc">Product Details</label>
                      <div id="editor1"></div>
                    </div>
                    <!--
                    <div class="form-group">
                      <label>Product Material</label>
                      <div class="mt-2">
                        <label class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Pipe</span>
                        </label>
                        <label class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Cover</span>
                        </label>
                        <label class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Table Top</span>
                        </label>
                        <label class="custom-control custom-checkbox custom-control-inline">
                          <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Glass</span>
                        </label>
                      </div>
                    </div>
                  -->
                    <div class="row pt-3">
                      <div class="col-sm-12">
                        <p>
                          <button type="submit" class="btn btn-space btn-lg btn-primary toggle-loading">Save</button>

                          <div class="be-spinner">
                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                              <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                            </svg>
                          </div>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">Product Image <span class="card-subtitle">Image Size : 1000 x 1000px</span></div>
                <div class="card-body">
                  <form action="#">
                    <div class="form-group row">
                      <label for="file-1" class="col-12 col-sm-3 col-form-label">Upload Image</label>
                      <div class="col-12 col-sm-6">
                        <input id="inputGambar" type="file" name="inputGambar" data-multiple-caption="{count} files selected" multiple class="inputfile">
                        <label for="inputGambar" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <p>
                          <button class="btn btn-primary btn-xl toggle-loading">Save</button>

                          <div class="be-spinner">
                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                              <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                            </svg>
                          </div>
                        </p>
                      </div>
                    </div>


                  </form>
                </div>
              </div>

              <div class="card be-loading card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">List Product Image</div>
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Main Image</th>
                        <th class="actions"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <!--
                      <tr>
                        <td>1</td>
                        <td><img src="../assets/img/product/Hearth.jpg" width="auto" height="100px"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td><img src="../assets/img/product/Hearth_Biru_Right.jpg" width="auto" height="100px"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td><img src="../assets/img/product/Hearth_Biru_Back.jpg" width="auto" height="100px"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td><img src="../assets/img/product/Hearth_Biru_Tumpuk.jpg" width="auto" height="100px"></td>
                        <td class="actions">
                          <a href="#" class="btn btn-space btn-secondary"><i class="icon mdi mdi-edit"></i></a>
                          <a href="#" class="btn btn-space btn-danger"><i class="icon mdi mdi-delete" style="color:white;"></i></a>
                        </td>
                      </tr>
                    -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
         <?php include ('navfooter.php') ?>
        </div>
      </div>
    </div>
    <?php include ('js.php') ?>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
        App.init();
        $('form').parsley();
      	App.formElements();
        App.loaders();
        App.textEditors();
      	//Runs prettify
      	prettyPrint();

      });
    </script>
  </body>
</html>
