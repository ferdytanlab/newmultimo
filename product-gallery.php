<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="projectName project details by Multimo.">
	<meta name="keywords" content="projectName, multimo, project">
	<title>projectName - MULTIMO</title>
	<!-- ========== CSS INCLUDES ========== -->
	<?php include ('css.php') ?>
	<style>
	html body {
	  background-color: white;
	}
	</style>
</head>
<body class="fixed-header">

	<div class="product-pup-up"></div>

	<?php include ('header.php') ?>


	<section class="slider-area slider-style-2">
				<div class="bend niceties preview-2">
					<div id="ensign-nivoslider" class="slides">
						<img src="assets/img/project/1Brighton.jpg">
            <img src="assets/img/project/2Brighton.jpg">
            <img src="assets/img/project/3Brighton.jpg">
					</div>
				</div>
			</section>
			<!-- SLIDER-AREA END -->


	<div class="section-overlay" ></div>

	<div class="single-page-base-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="mini-info-box">
						<h3>
							Description
						</h3>
						<p>
							Project Site : projectName<br>
							Year : 2017
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="mini-info-box">
						<h3>
							Product Link
						</h3>
						<div class="clearfix">
							<ul class="products">
                <li class="product">
                  <a href="product-detail.php">
                    <img src="assets/img/product/555.jpg" class="attachment-shop_catalog wp-post-image" alt="productName">
                  </a>
                  <div class="product-info">
                    <span class="brand-product" style="margin-bottom: 20px;">
                      <span class="the-product-status">
                        <span style="background-color: blue">brandname</span>
                      </span>
                    </span>
                    <a href="product-detail.php"><p>Seahorse Astro Stainless</p></a>
                    </div>
                </li>
								<li class="product">
                  <a href="product-detail.php">
                    <img src="assets/img/product/555.jpg" class="attachment-shop_catalog wp-post-image" alt="productName">
                  </a>
                  <div class="product-info">
                    <span class="brand-product" style="margin-bottom: 20px;">
                      <span class="the-product-status">
                        <span style="background-color: blue">brandname</span>
                      </span>
                    </span>
                    <a href="product-detail.php"><p>Seahorse Astro Stainless</p></a>
                    </div>
                </li>
								<li class="product">
                  <a href="product-detail.php">
                    <img src="assets/img/product/555.jpg" class="attachment-shop_catalog wp-post-image" alt="productName">
                  </a>
                  <div class="product-info">
                    <span class="brand-product" style="margin-bottom: 20px;">
                      <span class="the-product-status">
                        <span style="background-color: blue">brandname</span>
                      </span>
                    </span>
                    <a href="product-detail.php"><p>Seahorse Astro Stainless</p></a>
                    </div>
                </li>
								<li class="product">
                  <a href="product-detail.php">
                    <img src="assets/img/product/555.jpg" class="attachment-shop_catalog wp-post-image" alt="productName">
                  </a>
                  <div class="product-info">
                    <span class="brand-product" style="margin-bottom: 20px;">
                      <span class="the-product-status">
                        <span style="background-color: blue">brandname</span>
                      </span>
                    </span>
                    <a href="product-detail.php"><p>Seahorse Astro Stainless</p></a>
                    </div>
                </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="assets/js/vendor/jquery-1.12.0.min.js"></script>
	<script src="assets/lib/js/jquery.nivo.slider.js"></script>
	<script src="assets/lib/home.js"></script>
	<?php include ('footer.php') ?>
